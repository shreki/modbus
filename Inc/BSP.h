/**
 *****************************************************************************
 * @file
 * $URL: http://svn.stiebel-eltron.com/svn/iws/Projects/IWS3_MOD_393/branches/IWS3_MOD_393_NUCLEO_PORT/src/BSP/BSP.h $
 *
 * @brief   Board Support Package (BSP) is the interface component between the
 *          application and the different available hardware specific realization
 *          of  the full device
 *
 * @author  Ian Kamundi (IK) ,  Elektronik & Informatik OWL e.G.
 *
 * $Rev: 473 $
 *
 * $Date: 2018-05-03 13:10:08 +0200 (Do, 03 Mai 2018) $
 *
 * @note
 * Target   : uC, STM8, STM32 @n
 * Compiler : gcc 4.7, Cosmic Compiler
 *
 * @history
 * - Date       Author Ticket-ID: Ticket-Description @n
 *   Comment @n
 * - 2018-01-01 XXX    83: Testticket @n
 *   Initial Version @n
 *****************************************************************************
 * @copyright
 * Copyright (C) 2018 by Stiebel Eltron GmbH & Co. KG.@n
 * @n
 * This software is copyrighted by and is the sole property of Stiebel Eltron @n
 * GmbH & Co. KG.  All rights, title, ownership, or other interests @n
 * in the software remain the property of Stiebel Eltron GmbH & Co. KG. @n
 * This software may only be used in accordance with the corresponding @n
 * license agreement.  Any unauthorized use, duplication, transmission, @n
 * distribution, or disclosure of this software is expressly forbidden. @n
 * @n
 * This Copyright notice may not be removed or modified without prior @n
 * written consent of Stiebel Eltron GmbH & Co. KG. @n
 * @n
 * Stiebel Eltron GmbH & Co. KG reserves the right to modify this software @n
 * without notice. @n
 * @n
 * Stiebel Eltron GmbH & Co. KG @n
 * http://www.stiebel-eltron.de @n
 *****************************************************************************
 */

/* Define to prevent recursive inclusion ================================== */
#ifndef __GERAET_BSP_H__
#define __GERAET_BSP_H__

#include "hardware.h"
#include "appl_def.h"
#include "STE_CFG.h"
//#include "BSP_EEPROM.h"
//#include "BSP_WDG.h"
//#include "BSP_ADC.h"
#include "BSP_MODBUS.h"
//#include "BSP_CAN.h"
//#include "BSP_I2C.h"
//#include "BSP_EEPROM.h"
//#include "BSP_UART1.h"
//#include "BSP_Test.h"
//#include "BSP_Diag.h"

/* Public constants ======================================================= */

/* Public macros ========================================================== */

#define LUEFTER_STEUERREGISTER_SETZEN(x)      BSP_setCaptureCompareRegister(x)
#if BSP_USE_V850 == 1
#define BSP_WAIT_1_MICROSECOND() { NO_OPERATION() NO_OPERATION() NO_OPERATION() NO_OPERATION() }/** four times nop equals one microsecond on 4 MHz */
#else
#if BSP_USE_HAL_WINDOWS == 1
#define BSP_WAIT_1_MICROSECOND() {NO_OPERATION()}
#else
#if BSP_USE_STM32 == 1
#define BSP_WAIT_1_MICROSECOND()
// *Andreas*
// {HAL_Delay(1);} /**<  Use the Hal Systemclock  */
#endif
#endif
#endif

#if BSP_USE_STM32 == 1
extern void _Error_Handler(char *, int);
#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#endif

/**<  Following are CUBEMX generated Macros showing the relation of input or output GPIOS and the port names  */
#define DO_4_7_Pin GPIO_PIN_2
#define DO_4_7_GPIO_Port GPIOE
#define DO_4_6_Pin GPIO_PIN_3
#define DO_4_6_GPIO_Port GPIOE
#define DO_4_5_Pin GPIO_PIN_4
#define DO_4_5_GPIO_Port GPIOE
#define DO_4_4_Pin GPIO_PIN_5
#define DO_4_4_GPIO_Port GPIOE
#define AI_3_6_Pin GPIO_PIN_15
#define AI_3_6_GPIO_Port GPIOC
#define AI_3_7_Pin GPIO_PIN_0
#define AI_3_7_GPIO_Port GPIOC
#define AI_2_0_Pin GPIO_PIN_1
#define AI_2_0_GPIO_Port GPIOC
#define AI_2_1_Pin GPIO_PIN_2
#define AI_2_1_GPIO_Port GPIOC
#define AI_2_2_Pin GPIO_PIN_3
#define AI_2_2_GPIO_Port GPIOC
#define FIO_2_Puls_Pin GPIO_PIN_0
#define FIO_2_Puls_GPIO_Port GPIOA
#define ADC_SINGLE_Pin GPIO_PIN_1
#define ADC_SINGLE_GPIO_Port GPIOA
#define ADC_TRIPLE_Pin GPIO_PIN_2
#define ADC_TRIPLE_GPIO_Port GPIOA
#define FIO_1_PWM_Pin GPIO_PIN_0
#define FIO_1_PWM_GPIO_Port GPIOB
#define FIO_1_Puls_Pin GPIO_PIN_1
#define FIO_1_Puls_GPIO_Port GPIOB
#define DI_4_4_Pin GPIO_PIN_8
#define DI_4_4_GPIO_Port GPIOE
#define DI_4_3_Pin GPIO_PIN_9
#define DI_4_3_GPIO_Port GPIOE
#define DI_4_2_Pin GPIO_PIN_10
#define DI_4_2_GPIO_Port GPIOE
#define DI_4_1_Pin GPIO_PIN_11
#define DI_4_1_GPIO_Port GPIOE
#define DI_4_8_Pin GPIO_PIN_12
#define DI_4_8_GPIO_Port GPIOE
#define DI_4_7_Pin GPIO_PIN_13
#define DI_4_7_GPIO_Port GPIOE
#define DI_4_6_Pin GPIO_PIN_14
#define DI_4_6_GPIO_Port GPIOE
#define DI_4_5_Pin GPIO_PIN_15
#define DI_4_5_GPIO_Port GPIOE
#define DO_5_1_Pin GPIO_PIN_12
#define DO_5_1_GPIO_Port GPIOB
#define DO_5_2_Pin GPIO_PIN_13
#define DO_5_2_GPIO_Port GPIOB
#define DO_5_3_Pin GPIO_PIN_14
#define DO_5_3_GPIO_Port GPIOB
#define DO_1_9_Pin GPIO_PIN_9
#define DO_1_9_GPIO_Port GPIOD
#define DO_1_8_Pin GPIO_PIN_10
#define DO_1_8_GPIO_Port GPIOD
#define DO_1_7_Pin GPIO_PIN_11
#define DO_1_7_GPIO_Port GPIOD
#define DO_1_6_Pin GPIO_PIN_12
#define DO_1_6_GPIO_Port GPIOD
#define DO_1_5_Pin GPIO_PIN_13
#define DO_1_5_GPIO_Port GPIOD
#define DO_1_4_Pin GPIO_PIN_14
#define DO_1_4_GPIO_Port GPIOD
#define DO_1_3_Pin GPIO_PIN_15
#define DO_1_3_GPIO_Port GPIOD
#define DO_1_2_Pin GPIO_PIN_6
#define DO_1_2_GPIO_Port GPIOC
#define DO_1_1_Pin GPIO_PIN_7
#define DO_1_1_GPIO_Port GPIOC
#define PowerEN_12V_Pin GPIO_PIN_8
#define PowerEN_12V_GPIO_Port GPIOC
#define Synch_12V_Pin GPIO_PIN_9
#define Synch_12V_GPIO_Port GPIOC
#define DO_4_1_Pin GPIO_PIN_8
#define DO_4_1_GPIO_Port GPIOA
#define DO_4_3_Pin GPIO_PIN_10
#define DO_4_3_GPIO_Port GPIOA
#define DO_4_2_Pin GPIO_PIN_10
#define DO_4_2_GPIO_Port GPIOC
#define DI_1_0_Pin GPIO_PIN_11
#define DI_1_0_GPIO_Port GPIOC
#define DI_1_3_Pin GPIO_PIN_12
#define DI_1_3_GPIO_Port GPIOC
#define DI_1_1_Pin GPIO_PIN_0
#define DI_1_1_GPIO_Port GPIOD
#define DI_1_2_Pin GPIO_PIN_1
#define DI_1_2_GPIO_Port GPIOD
#define DO_3_2_Pin GPIO_PIN_2
#define DO_3_2_GPIO_Port GPIOD
#define DO_3_1_Pin GPIO_PIN_3
#define DO_3_1_GPIO_Port GPIOD
#define DO_3_3_Pin GPIO_PIN_7
#define DO_3_3_GPIO_Port GPIOD
#define DO_3_4_Pin GPIO_PIN_5
#define DO_3_4_GPIO_Port GPIOB
#define DO_2_1_Pin GPIO_PIN_6
#define DO_2_1_GPIO_Port GPIOB
#define DO_2_2_Pin GPIO_PIN_7
#define DO_2_2_GPIO_Port GPIOB
#define DO_2_3_Pin GPIO_PIN_0
#define DO_2_3_GPIO_Port GPIOE
#define DO_2_4_Pin GPIO_PIN_1
#define DO_2_4_GPIO_Port GPIOE

/* Public types =========================================================== */

/**
 * BSP_DigitalOutput_e_t Digital output ports
 */
typedef enum {
 BSP_DIGITAL_OUTPUT_1_1,         /**<  Relais 1: V850(PDL.0)  */
 BSP_DIGITAL_OUTPUT_1_2,         /**<  Relais 2: V850(PDL.1)  */
 BSP_DIGITAL_OUTPUT_1_3,         /**<  Relais 3: V850(PDL.2)  */
 BSP_DIGITAL_OUTPUT_1_4,         /**<  Relais 4: V850(PDL.3)  */
 BSP_DIGITAL_OUTPUT_1_5,         /**<  Relais 5: V850(PDL.4)  */
 BSP_DIGITAL_OUTPUT_1_6,         /**<  Relais 6: V850(PDL.5)  */
 BSP_DIGITAL_OUTPUT_1_7,         /**<  Relais 7: V850(PDL.6)  */
 BSP_DIGITAL_OUTPUT_1_8,         /**<  Relais 8: V850(PDL.7)  */
 BSP_DIGITAL_OUTPUT_1_9,         /**<  Relais 9: V850(PCT.3)  */
 BSP_DIGITAL_OUTPUT_2_1,         /**<  Schrittmotor Phase 1   */
 BSP_DIGITAL_OUTPUT_2_2,         /**<  Schrittmotor Phase 2   */
 BSP_DIGITAL_OUTPUT_2_3,         /**<  Schrittmotor Phase 3   */
 BSP_DIGITAL_OUTPUT_2_4,         /**<  Schrittmotor Phase 4   */
 BSP_DIGITAL_OUTPUT_3_1,         /**<  Schrittmotor EV Phase1 */
 BSP_DIGITAL_OUTPUT_3_2,         /**<  Schrittmotor EV Phase2 */
 BSP_DIGITAL_OUTPUT_3_3,         /**<  Schrittmotor EV Phase3 */
 BSP_DIGITAL_OUTPUT_3_4,         /**<  Schrittmotor EV Phase4 */
 BSP_DIGITAL_OUTPUT_4_1,         /**<  LED FATAL V850(P9.0)  */
 BSP_DIGITAL_OUTPUT_4_2,         /**<  LED BUS   V850(P9.1)  */
 BSP_DIGITAL_OUTPUT_4_3,         /**<  LED AUSSEN STELLUNG V850(P5.5) */
 BSP_DIGITAL_OUTPUT_4_4,         /**<  MUX_CHANNEL_1     */
 BSP_DIGITAL_OUTPUT_4_5,         /**<  MUX_CHANNEL_2     */
 BSP_DIGITAL_OUTPUT_4_6,         /**<  MUX_CHANNEL_3     */
 BSP_DIGITAL_OUTPUT_4_7,         /**<  MUX_CHANNEL_4     */
 BSP_DIGITAL_OUTPUT_5_1,         /**<  EEPROM_WC */
 BSP_DIGITAL_OUTPUT_5_2,         /**<  EEPROM_SCL */
 BSP_DIGITAL_OUTPUT_5_3          /**<  EEPROM_SDA */
} BSP_e_DigitalOutput_t;         /**<  Application digital outputs */

/**
 * BSP_DigitalInput_e_t Digital input ports
 */
typedef enum
{
 BSP_DIGITAL_INPUT_1_0,           /**< Opto Coupler  HD      V850(P7.1)   */
 BSP_DIGITAL_INPUT_1_1,           /**< Opto Coupler  PULS    V850(P7.2)   */
 BSP_DIGITAL_INPUT_1_2,           /**< Opto Coupler  SAMMEL  V850(PCS.0)  */
 BSP_DIGITAL_INPUT_1_3,           /**< Opto Coupler  ABTAUEN V850(PCS.1)  */
 BSP_DIGITAL_INPUT_4_1,           /**< DIP Maschine Type V850(P0.0)  */
 BSP_DIGITAL_INPUT_4_2,           /**< DIP Maschine Type V850(P0.1)  */
 BSP_DIGITAL_INPUT_4_3,           /**< DIP Maschine Type V850(P0.2)  */
 BSP_DIGITAL_INPUT_4_4,           /**< DIP Maschine Type V850(P0.3)  */
 BSP_DIGITAL_INPUT_4_5,           /**< DIP Betriebsart   V850(P0.4)  */
 BSP_DIGITAL_INPUT_4_6,           /**< DIP Betriebsart   V850(P0.6)  */
 BSP_DIGITAL_INPUT_4_7,           /**< DIP Betriebsart   V850(P3.8)  */
 BSP_DIGITAL_INPUT_4_8,           /**< DIP Betriebsart   V850(P3.9)  */
 BSP_DIGITAL_INPUT_4_9            /**< DIP Reset Key     V850(P9.13) */
}
BSP_e_DigitalInput_t;             /**<  Application digital inputs */

/**
 * BSP_e_Timer_Event_t events
 */
typedef enum
{
  BSP_TIMER_EVENT_ERROR,     /**< Generic error */
  BSP_TIMER_EVENT_50MS,      /**< Event for 50ms timer */
  BSP_TIMER_EVENT_100MS,     /**< Event for 100ms timer */
  BSP_TIMER_EVENT_500MS,     /**< Event for 500ms timer */
  BSP_TIMER_EVENT_1000MS,    /**< Event for 1000ms timer*/
 } BSP_e_Timer_Event_t;


/**
  * BSP_e_ExternalPin_Event_t events
  */
typedef enum
{
  BSP_EXTERNAL_EVENT_INTP5,      /**< External Interrupt on PinINTP5 */
  BSP_EXTERNAL_EVENT_INTP6,      /**< External Interrupt on PinINTP5  */
  BSP_EXTERNAL_EVENT_INTP_UNUSED  /**< unused external interrupts       */
} BSP_e_ExternalPin_Event_t;

/** * Actor States
 */
typedef enum {
   BSP_ACTOR_OFF,              /**<  Logical level Low of a port pin */
   BSP_ACTOR_ON                /**<  Logical level high of a port pin */
} BSP_e_ActorState_t;

/* Public variables ======================================================= */

/* Public functions ======================================================= */
extern void BSP_waitMs(uint16_t u16_MillisecondsDelay);
extern void BSP_init ( void );

extern uint8_t BSP_getDigitalInputState( BSP_e_DigitalInput_t e_DigitalInput );

extern uint8_t BSP_getDigitalOutputState( BSP_e_DigitalOutput_t e_DigitalOutput );

extern uint8_t BSP_setDigitalOutputState( BSP_e_DigitalOutput_t e_DigitalOutput, BSP_e_ActorState_t e_ActorStateToSet );

extern uint8_t BSP_setLedOutputState( BSP_e_DigitalOutput_t e_DigitalOutput, BSP_e_ActorState_t e_ActorStateToSet, uint8_t *u8_led_status );

extern void BSP_setCaptureCompareRegister( uint16_t u16_Value );

extern void BSP_setInterruptEnable( void );

extern  void BSP_setInterruptDisable(void);

extern bool BSP_addTimerEventListenerFunction( CFG_CODE_MEMORY_MODEL void (*p_EventListenerFunction)( BSP_e_Timer_Event_t ) );

extern bool BSP_addExternalPinEventListenerFunction( CFG_CODE_MEMORY_MODEL void (*p_EventListenerFunction)( BSP_e_ExternalPin_Event_t ) );
#endif  /* #ifndef __GERAET_BSP_H__ */

/* end of BSP.h ==================================================== */
