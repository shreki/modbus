/**
 *****************************************************************************
 * @file
 * $URL: http://svn.stiebel-eltron.com/svn/iws/Projects/IWS3_MOD_393/branches/IWS3_MOD_393_NUCLEO_PORT/src/BSP/BSP.h $
 *
 * @brief   Board Support Package (BSP_MODBUS) is the interface component between the
 *          application and the MODBUS functions
 *
 * @author  Ian Kamundi (IK) ,  Elektronik & Informatik OWL e.G.
 *
 * $Rev: 473 $
 *
 * $Date: 2018-05-03 13:10:08 +0200 (Do, 03 Mai 2018) $
 *
 * @note
 * Target   : uC, STM8, STM32 @n
 * Compiler : gcc 4.7, Cosmic Compiler
 *
 * @history
 * - Date       Author Ticket-ID: Ticket-Description @n
 *   Comment @n
 * - 2018-01-01 XXX    83: Testticket @n
 *   Initial Version @n
 *****************************************************************************
 * @copyright
 * Copyright (C) 2018 by Stiebel Eltron GmbH & Co. KG.@n
 * @n
 * This software is copyrighted by and is the sole property of Stiebel Eltron @n
 * GmbH & Co. KG.  All rights, title, ownership, or other interests @n
 * in the software remain the property of Stiebel Eltron GmbH & Co. KG. @n
 * This software may only be used in accordance with the corresponding @n
 * license agreement.  Any unauthorized use, duplication, transmission, @n
 * distribution, or disclosure of this software is expressly forbidden. @n
 * @n
 * This Copyright notice may not be removed or modified without prior @n
 * written consent of Stiebel Eltron GmbH & Co. KG. @n
 * @n
 * Stiebel Eltron GmbH & Co. KG reserves the right to modify this software @n
 * without notice. @n
 * @n
 * Stiebel Eltron GmbH & Co. KG @n
 * http://www.stiebel-eltron.de @n
 *****************************************************************************
 */

/* Define to prevent recursive inclusion ================================== */
#ifndef __BSP_MODBUS_H
#define __BSP_MODBUS_H

#include "STE_CFG.h"
//#include "BSP_WDG.h"
#include "BSP.h"
#include "data_link_layer_common_ext.h"  /**< mod_bus_comm_state_enm */
#include "data_link_layer_common.h"     /**< mod_bus_data_def_strct */

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_ll_bus.h"
#include "stm32l4xx_ll_rcc.h"
#include "stm32l4xx_ll_system.h"
#include "stm32l4xx_ll_utils.h"
#include "stm32l4xx_ll_gpio.h"
#include "stm32l4xx_ll_exti.h"
#include "stm32l4xx_ll_usart.h"
#include "stm32l4xx_ll_pwr.h"
#if defined(USE_FULL_ASSERT)
#include "stm32_assert.h"
#endif /* USE_FULL_ASSERT */

/* Public constants ======================================================= */
// Ein gesetztes Bit bedeutet, dass die zugehörige Stelle im Source durchlaufen wurde
#define MOD_BUS_DBG_DATA_LINK_INIT_UART_ENTRY             0x0001
#define MOD_BUS_DBG_DATA_LINK_INIT_UART_EXIT              0x0002
#define MOD_BUS_DBG_DATA_LINK_UART_RX_ISR                 0x0004
#define MOD_BUS_DBG_DATA_LINK_UART_STATE_ISR              0x0008
#define MOD_BUS_DBG_DATA_LINK_STATE_IDLE                  0x0010
#define MOD_BUS_DBG_DATA_LINK_STATE_COM_REQ               0x0020
#define MOD_BUS_DBG_DATA_LINK_STATE_COM_PROGR             0x0040
#define MOD_BUS_DBG_DATA_LINK_INIT_ENTRY                  0x0080
#define MOD_BUS_DBG_DATA_LINK_INIT_EXIT                   0x0100
#define MOD_BUS_DBG_DATA_LINK_REQ_ENTRY                   0x0200
#define MOD_BUS_DBG_DATA_LINK_REQ_EXIT                    0x0400
#define MOD_BUS_DBG_DATA_LINK_CHK_ENTRY                   0x0800
#define MOD_BUS_DBG_DATA_LINK_CHK_EXIT                    0x1000
#define MOD_BUS_DBG_DATA_LINK_UART_ERR                    0x2000
#define MOD_BUS_DBG_DATA_LINK_TIMEOUT_ERR                 0x4000
#define MOD_BUS_DBG_DATA_LINK_FRAME_PAUSE_ERR             0x8000

/* Public macros ========================================================== */
/**< These macros created the interface between the  MODBUS_DATA_LAYER_V850 <-> APPLICATION  */
#define set_mod_bus_data_ptr(x)                   BSP_MODBUS_setDataPtr((x))
#define set_interbyte_timeout(x)                  BSP_MODBUS_setInterbyteTimeout((x))
#define mod_bus_data_link_layer_set_dbg_state(x)  BSP_MODBUS_setDebugState((x))
#define mod_bus_data_link_layer_get_dbg_state(x)  BSP_MODBUS_getDebugState(x)
#define init_mod_bus_uart(x, y)                   BSP_MODBUS_initUart((x), (y))
#define mod_bus_status_maschine()                 BSP_Modbus_stateMachine()
#define modbus_100_ms_aktionen()                  BSP_MODBUS_100msAktionen()
#define set_frame_pause(x)                        BSP_MODBUS_setFramePause((x))

/* USART2 instance is used. (TX on PA.02, RX on PA.03)
   (please ensure that USART communication between the target MCU and ST-LINK MCU is properly enabled
    on HW board in order to support Virtual Com Port) */

#define USARTx_INSTANCE               USART2
#define USARTx_CLK_ENABLE()           LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART2)
#define USARTx_CLK_SOURCE()           LL_RCC_SetUSARTClockSource(LL_RCC_USART2_CLKSOURCE_PCLK1)
#define USARTx_IRQn                   USART2_IRQn
#define USARTx_IRQHandler             USART2_IRQHandler

#define USARTx_GPIO_CLK_ENABLE()      LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOA)   /* Enable the peripheral clock of GPIOA */
#define USARTx_TX_PIN                 LL_GPIO_PIN_2
#define USARTx_TX_GPIO_PORT           GPIOA
#define USARTx_SET_TX_GPIO_AF()       LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_2, LL_GPIO_AF_7)
#define USARTx_RX_PIN                 LL_GPIO_PIN_3
#define USARTx_RX_GPIO_PORT           GPIOA
#define USARTx_SET_RX_GPIO_AF()       LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_3, LL_GPIO_AF_7)

/* Public types =========================================================== */
typedef mod_bus_data_def_strct     BSP_MODBUS_st_ModBusData_t;
typedef baudrate_def_enm           BSP_MODBUS_e_BaudRate_t;
typedef parity_def_enm             BSP_MODBUS_e_Parity_t;

/* Public variables ======================================================= */

/* Public functions ======================================================= */
extern   int8_t    BSP_MODBUS_initUart            (BSP_MODBUS_e_BaudRate_t    e_Baudrate,  BSP_MODBUS_e_Parity_t   e_Parity);
extern   void      BSP_MODBUS_init        (void);
extern   void      BSP_Modbus_stateMachine(void);
extern   uint16    BSP_MODBUS_getDebugState(void);
extern   void      BSP_MODBUS_setDebugState(uint16 state_u16);
extern   void      BSP_MODBUS_setInterbyteTimeout (uint16_t   u16_NewInterbyteTimeout);
extern   void      BSP_MODBUS_100msAktionen  (void);
extern   uint16    BSP_MODBUS_get100msCounter(void);
extern   void      BSP_MODBUS_setDataPtr     (BSP_MODBUS_st_ModBusData_t* pst_NewModBusData);
extern   void      BSP_MODBUS_setFramePause(uint16 u16_NewFramePauseTimerValue);

/* end of BSP_MODBUS.h ==================================================== */
#endif //__BSP_MODBUS_H
