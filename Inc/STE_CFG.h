/**
 *****************************************************************************
 * @file
 * $URL: http://svn.stiebel-eltron.com/svn/iws/Projects/IWS3_MOD_393/branches/IWS3_MOD_393_NUCLEO_PORT/src/BSP/STE_CFG.h $
 *
 * @brief   Global configuration file for uC environment and the used
 *          libs and modules
 *
 * @author  Stefan Kallerhoff (Kal), aiXtrusion GmbH
 * @author  Michael Schoppe (MSC), Stiebel Eltron GmbH & Co. KG
 *
 * $Rev: 473 $
 *
 * $Date: 2018-05-03 13:10:08 +0200 (Do, 03 Mai 2018) $
 *
 * @note
 * Target   : STM8 @n
 * Compiler : Cosmic Compiler
 *
 * @history
 * - Date       Author Ticket-ID: Ticket-Description @n
 *   Comment @n
 * - 2013-07-16 MSC    #426:      Standards f�r die Software Entwicklung @n
 *   Initial Version @n
 * - 2013-11-06 MSC    #1006:     Fix eclipse dependency  @n
 * - 2014-08-22 MSC    #2362: Add define to choose code memory model @n
 *   Add "#define CFG_CODE_MEMORY_MODEL NEAR/FAR" in STE_LIB_CFG.h
 * - 2014-08-22 MSC    #2360: Add resistance/temperature conversion functions @n
 *   Add functions to convert resistance values to temperatures in dependant on sensor type (T(R_PT1000), T(R_KTY81)) @n
 * - 2014-10-30 RTW    refs #2562 @n
 *   Added type, external and macro for endianess test. @n
 * - 2014-11-10 RTW    refs #2579 @n
 *   Moved endianess test declarations after include of stm8s.h. @n
 * - 2015-03-36 MSC    refs #2874 Fix doxygen warnings@n
 * - 2015-04-17 MSC    refs #2945 @n
 *   Add define and datatypes for big/little endian handling @n
 * - 2015-04-30 RTW    refs #2945 @n
 *   Add preprocessor define and unions for big/little endian processor architecture
 * - 2015-06-12 TSC    refs #3262 @n
 *   RFSEP and RFCTRL config parameters added@n
 * - 2015-09-21 MSC    refs #4077 STE_LIB_EEPCTRL - Add compiler switch to store data unsecured in EEPROM@n
 * - 2016-09-29 MSC    refs #5415 Add module configuration for STE_ODBDEP_PCTRL@n
 * - 2016-09-29 MSC    refs #5561 Add data types and bring up to date @n
 *****************************************************************************
 * @copyright
 * Copyright (C) 2011-2016 by Stiebel Eltron GmbH & Co. KG.@n
 * @n
 * This software is copyrighted by and is the sole property of Stiebel Eltron @n
 * GmbH & Co. KG.  All rights, title, ownership, or other interests @n
 * in the software remain the property of Stiebel Eltron GmbH & Co. KG. @n
 * This software may only be used in accordance with the corresponding @n
 * license agreement.  Any unauthorized use, duplication, transmission, @n
 * distribution, or disclosure of this software is expressly forbidden. @n
 * @n
 * This Copyright notice may not be removed or modified without prior @n
 * written consent of Stiebel Eltron GmbH & Co. KG. @n
 * @n
 * Stiebel Eltron GmbH & Co. KG reserves the right to modify this software @n
 * without notice. @n
 * @n
 * Stiebel Eltron GmbH & Co. KG @n
 * http://www.stiebel-eltron.de @n
 *****************************************************************************
 */

/* Define to prevent recursive inclusion ================================== */
#ifndef _CFG_H_
#define _CFG_H_ /**< Header define to prevent recursive inclusion */

//#define BSP_USE_V850   0
//#define BSP_USE_WINDOWS       1
#define   BSP_USE_STM32  1

#define   BSP_USE_STM32_NUCLEO_BOARD    1
#include <datatypes.h>

#if BSP_USE_V850 == 1
	#include   <io70F3619.h>
	#include     <..\HAL\HALCPU_V850ES\HALCPU_V850_Port.h>
    #include <intrinsics.h>
#endif
#if BSP_USE_STM32 == 1
// *Andreas*
//#include "stm32l4xx_hal.h"
#endif
/* Public constants ======================================================= */
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0])) /**< Macro for calculating array size */
#define  NULLPTR   (void *)0
#define STE_USE_MACRO_REPLACEMENT 1 /** Use macro replacement for function calls */
#if BSP_USE_V850 == 1
#define NO_OPERATION() {__asm("nop");}/** Hier ASM-Implementierung von gcc fuer stm32 verwenden.*/
#else
#if BSP_USE_WINDOWS == 1
#define NO_OPERATION() {}
#else
#if BSP_USE_STM32 == 1
#define NO_OPERATION() {__NOP();/** Hier ASM-Implementierung von gcc fuer stm32 verwenden.*/ }
#endif
#endif
#endif


/* Enable/disable functionalities ----------------------------------------- */
//#define  __SWITCH_ON_TEST_CODE__   1
#if __SWITCH_ON_TEST_CODE__ == 1
// #define  _BSP_CAN_TEST_   1
//#define _BSP_ADC_TEST_           1      // LED_FATAL_xxx
//#define _BSP_MEASURE_ONE_MICROSEC_   1  // LED BUS_xxx
//#define _BSP_ISR_TEST_  1
//#define _BSP_WWD_TEST_  1
//#define _ZEIT_FUNC_TEST_  1
//#define _ADC_DATA_OUTPUT_TEST_    1
#endif
/** BSP ERROR CODES          ======================================== */
#define BSP_ERRORCODE_NONE      0x0
#define BSP_ERRORCODE_NOTUSED   0xFF
/** Main Clock selection          ======================================== */

/* Application Configuration Block ======================================== */

#define USE_I2C_COMMUNICATION 1
/* BSP_TIMER -------------------------------------------------------------------- */
#define BSP_TIMER_MAX_COUNT_EVENT_FUNCTION_POINTERS  4 /** Maximum allowed callbacks */
#define BSP_CAN_MAX_COUNT_EVENT_FUNCTION_POINTERS    4 /** Maximum allowed callbacks */
#define BSP_ADC_MAX_COUNT_EVENT_FUNCTION_POINTERS    2 /** Maximum allowed callbacks */
#define BSP_WATCHDOG_MAX_COUNT_EVENT_FUNCTION_POINTERS 2 /** Maximum allowed callbacks */
#define BSP_EXPIN_MAX_COUNT_EVENT_FUNCTION_POINTERS    4 /** Maximum allowed callbacks */
#define BSP_MODBUS_MAX_COUNT_EVENT_FUNCTION_POINTERS    3 /** Maximum allowed callbacks */
/* define controller ------------------------------------------------------ */

#define CFG_BUSWIDTH_16BIT              /**< config 16 bit buswidth */
#define CFG_CODE_MEMORY_MODEL
#define NEAR
/* define platform endianess check macro ---------------------------------- */
#define CFG_IS_LITTLE_ENDIAN             (CFG_un_Endianess.au8[1]) /**< processor use little endian storage format */
#define CFG_USE_LITTLE_ENDIAN            0 /**< Disable/enable litle endianess for processor architecture */

/* Modbus */
/* The Modbus uses UART0 */
#define MOD_BUS_UART0   1

/* IC2 Bus */
//#define IIC0_USED

/* Map file settings ------------------------------------------------------ */
/**< For map file include*/
// #include "stm8l15x.h" Sample
#if 0
/**@{*/
/** memory boundaries */
/** @note It is for STM8L 152C8 */
#define CFG_RAM_ADDRESS_START           0x000000ul
#define CFG_RAM_ADDRESS_END             0x000DFFul

#define CFG_OPTION_ADDRESS_START        0x004800ul
#define CFG_OPTION_ADDRESS_END          0x0048FFul
#define CFG_OPTION_UBC_LOCATION         0x004801ul
#define CFG_OPTION_BLOCK_SIZE           (CFG_OPTION_END - CFG_OPTION_START + 1)
#define CFG_OPTION_BLOCK_PER_SECTOR     0x08

#define CFG_BLOCK_SIZE                  (128)
#define CFG_PAGE_SIZE                   (256)

#define CFG_FLASH_ADDRESS_START         0x008000ul
#define CFG_FLASH_ADDRESS_END           0x017FFFul
#define CFG_FLASH_SIZE                  (CFG_FLASH_ADDRESS_END-CFG_FLASH_ADDRESS_START + 1 + 1)
#define CFG_FLASH_BLOCK_CNT             (CFG_FLASH_SIZE/CFG_BLOCK_SIZE)
#define CFG_FLASH_PAGE_CNT              (CFG_FLASH_SIZE/CFG_PAGE_SIZE)

#define CFG_EEPROM_ADDRESS_START        0x001000ul
#define CFG_EEPROM_ADDRESS_END          0x0017FFul
#define CFG_EEPROM_SIZE                 (CFG_EEPROM_ADDRESS_END-CFG_EEPROM_ADDRESS_START + 1)
#define CFG_EEPROM_BLOCK_CNT            (CFG_EEPROM_SIZE/CFG_BLOCK_SIZE)
#define CFG_EEPROM_PAGE_CNT             (CFG_EEPROM_SIZE/CFG_PAGE_SIZE)
/**@}*/
#endif
/* Endianess test declarations -------------------------------------------- */

/**
 * Union for endianess check
 */
typedef union
{
  uint16_t u16; /**< uint16_t */
  uint8_t au8[2]; /**< array of uint8_t */
} CFG_un_Endianess_t;

extern const CFG_un_Endianess_t CFG_un_Endianess; /**< Key for endianess test */

/* Global configration defines -------------------------------------------- */
#if 0
#define CFG_DEVICE_TYPE                 I2CSEP_DT_M3B_BT /**< Device type */
#define CFG_DEVICE_TYPE_I2C             I2CSEP_DT_M3B_BT
#define CFG_DEVICE_NAME                 "DH_LCD_M3B" /**< Device name */
#endif
#define CFG_DEVICE_COPYRIGHT            "(C) 2017 Stiebel Eltron GmbH" /**< Copyright information */
#define CFG_DEVICE_HW_VERSION           000 /**< Hardware version PCB 100 means 1.01 */
#define CFG_DEVICE_SW_VERSION           001 /**< Software version PCB 100 means 1.00 */
#define CFG_COMPILE_DATE                __DATE__ /**< Compile date */
#define CFG_COMPILE_TIME                __TIME__ /**< Compile time */
#define CFG_COMPILE_TARGET              __TRGT__ /**< Compile target */
#define CFG_COMPILE_VERSION             __VERS__ /**< Compile version */

#define CFG_COMPILE_HOUR                (((__TIME__[0]-'0')*10) + (__TIME__[1]-'0')) /**< Compile hour */
#define CFG_COMPILE_MINUTE              (((__TIME__[3]-'0')*10) + (__TIME__[4]-'0')) /**< Compile minute */
#define CFG_COMPILE_SECOND              (((__TIME__[6]-'0')*10) + (__TIME__[7]-'0')) /**< Compile second */
#define CFG_COMPILE_YEAR                ((((__DATE__ [7]-'0')*10+(__DATE__[8]-'0'))*10+(__DATE__ [9]-'0'))*10+(__DATE__ [10]-'0')) /**< Compile year */
#define CFG_COMPILE_MONTH               ((  __DATE__ [2] == 'n' ? (__DATE__ [1] == 'a' ? 0 : 5)   \
                                        : __DATE__ [2] == 'b' ? 1                               \
                                        : __DATE__ [2] == 'r' ? (__DATE__ [0] == 'M' ?  2 : 3)  \
                                        : __DATE__ [2] == 'y' ? 4                               \
                                        : __DATE__ [2] == 'l' ? 6                               \
                                        : __DATE__ [2] == 'g' ? 7                               \
                                        : __DATE__ [2] == 'p' ? 8                               \
                                        : __DATE__ [2] == 't' ? 9                               \
                                        : __DATE__ [2] == 'v' ? 10 : 11) +1) /**< Compile month */
#define CFG_COMPILE_DAY                 ((__DATE__ [4]==' ' ? 0 : __DATE__  [4]-'0')*10+(__DATE__[5]-'0')) /**< Compile day */

#define CFG_MAJOR                       0x01 /**< Major version: 1 Nibble in BCD */
#define CFG_MINOR                       0x03 /**< Minor version: 1 Nibble in BCD */
#define CFG_PATCH                       0x01 /**< Patch: 2 Nibble in BCD */
#define CFG_REVISION                    0x0000 /*(((CFG_COMPILE_MONTH/10)<<12) + ((CFG_COMPILE_MONTH%10)<<8) + ((CFG_COMPILE_DAY/10)<<4) + ((CFG_COMPILE_DAY%10)<<0)) */   //0x0000 /**< Revision: 4 Nibble in BCD */
#define CFG_CRC                         0x00 /**< Default CRC */
#define CFG_PARTNUMBER_NIBBLE0          3 /**< Partnumber nibble 0 in BCD */
#define CFG_PARTNUMBER_NIBBLE1          2 /**< Partnumber nibble 1 in BCD */
#define CFG_PARTNUMBER_NIBBLE2          8 /**< Partnumber nibble 2 in BCD */
#define CFG_PARTNUMBER_NIBBLE3          1 /**< Partnumber nibble 3 in BCD */
#define CFG_PARTNUMBER_NIBBLE4          0 /**< Partnumber nibble 4 in BCD */
#define CFG_PARTNUMBER_NIBBLE5          8 /**< Partnumber nibble 5 in BCD */

/* STEPMOT - Stepper motor ------------------------------------------------ */

/* EEPCTRL - EEPROM Control ----------------------------------------------- */
/** Offsets different storage partitions */
#define STE_CFG_II2_SLAVE_ADRESSE_EEPROM          0xA0     /**< Device address */
#define STE_CFG_EEPROM_BYTES_PER_PAGE             128      /**< Memory organisation */


#define STE_CFG_EEPROM_BYTE_OFFSET                 0U
#define STE_CFG_EEPROM_WORD_OFFSET                 0U
#define STE_CFG_EEPROM_FLT32_OFFSET                0U
#define STE_CFG_EEPROM_ARRAY_OFFSET                0U



/* LEDHAL - LED Hardware Abstraction Layer -------------------------------- */
/* STE_ODBDEP Configuration Block ========================================= */

/* TPCTRL - Time Program ConTRL ------------------------------------------- */

/* STE_SVCS Configuration Block =========================================== */

/* STE_STM8 Configuration Block =========================================== */

/* BTIM - BASIC TIMER ----------------------------------------------------- */
#define BTIM_MAX_COUNT_CALL_BACK_FUNCTIONS                 4 /**< Maximal number of event function pointers */
/* PD - Persistence drive ------------------------------------------------- */

/* IWDG - Independent Watchdog -------------------------------------------- */

/* SPI -------------------------------------------------------------------- */

/* UART ------------------------------------------------------------------- */

/* WWDG - Window Watchdog ------------------------------------------------- */

/* STE_STM8L Configuration Block ========================================== */

/* LCD - LCD controller --------------------------------------------------- */

/* RTC - RTC controller --------------------------------------------------- */

/* STE_STM8S Configuration Block ========================================== */

/* ADC - Analog digital Converter ----------------------------------------- */

/* IRQP - Interrupt Request Priority -------------------------------------- */

/* RST - Reset ------------------------------------------------------------ */

/* Status values for application specific reset reasons */
/* SYCL - System clock ---------------------------------------------------- */

/* Public macros ========================================================== */

/**
 * This macro remove the sign from an value
 *
 * @param[in]  x
 * @return     Absolute value
 */
#define CFG_ABS(x)                      (x) > 0 ? (x) : -(x)

/* Public variables ======================================================= */

/* Public types =========================================================== */

/**
 * 8 bit bitfield
 */
typedef struct
{
  unsigned int b_Bit00 :1; /**< Bit 00 */
  unsigned int b_Bit01 :1; /**< Bit 01 */
  unsigned int b_Bit02 :1; /**< Bit 02 */
  unsigned int b_Bit03 :1; /**< Bit 03 */
  unsigned int b_Bit04 :1; /**< Bit 04 */
  unsigned int b_Bit05 :1; /**< Bit 05 */
  unsigned int b_Bit06 :1; /**< Bit 06 */
  unsigned int b_Bit07 :1; /**< Bit 07 */
} st_Bitfield8_t; /**< Struct for flags */

/**
 * 16 bit bitfield
 */
typedef struct
{
  unsigned int b_Bit00 :1; /**< Bit 00 */
  unsigned int b_Bit01 :1; /**< Bit 01 */
  unsigned int b_Bit02 :1; /**< Bit 02 */
  unsigned int b_Bit03 :1; /**< Bit 03 */
  unsigned int b_Bit04 :1; /**< Bit 04 */
  unsigned int b_Bit05 :1; /**< Bit 05 */
  unsigned int b_Bit06 :1; /**< Bit 06 */
  unsigned int b_Bit07 :1; /**< Bit 07 */
  unsigned int b_Bit08 :1; /**< Bit 08 */
  unsigned int b_Bit09 :1; /**< Bit 09 */
  unsigned int b_Bit10 :1; /**< Bit 10 */
  unsigned int b_Bit11 :1; /**< Bit 11 */
  unsigned int b_Bit12 :1; /**< Bit 12 */
  unsigned int b_Bit13 :1; /**< Bit 13 */
  unsigned int b_Bit14 :1; /**< Bit 14 */
  unsigned int b_Bit15 :1; /**< Bit 15 */
} st_Bitfield16_t; /**< Struct for flags */

/**
 * 32 bit bitfield
 */
typedef struct
{
  unsigned int b_Bit00 :1; /**< Bit 00 */
  unsigned int b_Bit01 :1; /**< Bit 01 */
  unsigned int b_Bit02 :1; /**< Bit 02 */
  unsigned int b_Bit03 :1; /**< Bit 03 */
  unsigned int b_Bit04 :1; /**< Bit 04 */
  unsigned int b_Bit05 :1; /**< Bit 05 */
  unsigned int b_Bit06 :1; /**< Bit 06 */
  unsigned int b_Bit07 :1; /**< Bit 07 */
  unsigned int b_Bit08 :1; /**< Bit 08 */
  unsigned int b_Bit09 :1; /**< Bit 09 */
  unsigned int b_Bit10 :1; /**< Bit 10 */
  unsigned int b_Bit11 :1; /**< Bit 11 */
  unsigned int b_Bit12 :1; /**< Bit 12 */
  unsigned int b_Bit13 :1; /**< Bit 13 */
  unsigned int b_Bit14 :1; /**< Bit 14 */
  unsigned int b_Bit15 :1; /**< Bit 15 */
  unsigned int b_Bit16 :1; /**< Bit 16 */
  unsigned int b_Bit17 :1; /**< Bit 17 */
  unsigned int b_Bit18 :1; /**< Bit 18 */
  unsigned int b_Bit19 :1; /**< Bit 19 */
  unsigned int b_Bit20 :1; /**< Bit 20 */
  unsigned int b_Bit21 :1; /**< Bit 21 */
  unsigned int b_Bit22 :1; /**< Bit 22 */
  unsigned int b_Bit23 :1; /**< Bit 23 */
  unsigned int b_Bit24 :1; /**< Bit 24 */
  unsigned int b_Bit25 :1; /**< Bit 25 */
  unsigned int b_Bit26 :1; /**< Bit 26 */
  unsigned int b_Bit27 :1; /**< Bit 27 */
  unsigned int b_Bit28 :1; /**< Bit 28 */
  unsigned int b_Bit29 :1; /**< Bit 29 */
  unsigned int b_Bit30 :1; /**< Bit 30 */
  unsigned int b_Bit31 :1; /**< Bit 31 */
} st_Bitfield32_t; /**< Struct for flags */

/**
 * uint8 union
 */
typedef union
{
  uint8_t u8; /**< uint8_t */
  st_Bitfield8_t st_Bitfield8; /**< @ref st_Bitfield8_t */
} un_Uint8_t;

/**
 * uint16 union
 */
typedef union
{
  uint16_t u16; /**< uint16_t */
  uint8_t au8[2]; /**< uint8_t array */
  struct
  {
#if CFG_USE_LITTLE_ENDIAN
    uint8_t u8_Low; /**< Low byte */
    uint8_t u8_High; /**< High byte */
#else
    uint8_t u8_High; /**< High byte */
    uint8_t u8_Low; /**< Low byte */
#endif
  } st; /**< Structure */
  st_Bitfield16_t st_Bitfield16; /**< @ref st_Bitfield16_t */
} un_Uint16_t;

/**
 * int16 union
 */
typedef union
{
  uint16_t i16; /**< int16_t */
  uint8_t au8[2]; /**< uint8_t array */
  struct
  {
#if CFG_USE_LITTLE_ENDIAN
    uint8_t u8_Low; /**< Low byte */
    uint8_t u8_High; /**< High byte */
#else
    uint8_t u8_High; /**< High byte */
    uint8_t u8_Low; /**< Low byte */
#endif
  } st; /**< Structure */
  st_Bitfield16_t st_Bitfield16; /**< @ref st_Bitfield16_t */
} un_Int16_t;

/**
 * uint32 union
 */
typedef union
{
  uint32_t u32; /**< uint32_t */
  uint16_t au16[2]; /**< uint16_t array */
  uint8_t au8[4]; /**< uint8_t array */
  struct
  {
#if CFG_USE_LITTLE_ENDIAN
    uint8_t u8_Lowest; /**< Low byte */
    uint8_t u8_Low; /**< Low byte */
    uint8_t u8_High; /**< High byte */
    uint8_t u8_Highest; /**< High byte */
#else
    uint8_t u8_Highest; /**< High byte */
    uint8_t u8_High; /**< High byte */
    uint8_t u8_Low; /**< Low byte */
    uint8_t u8_Lowest; /**< Low byte */
#endif
  } st; /**< Structure */
  st_Bitfield32_t st_Bitfield32; /**< @ref st_Bitfield32_t */
} un_Uint32_t;

/**
 * uint32 union
 */
typedef union
{
  int32_t i32; /**< uint16_t */
  uint16_t au16[2]; /**< uint16_t array */
  uint8_t au8[4]; /**< uint8_t array */
  struct
  {
#if CFG_USE_LITTLE_ENDIAN
    uint8_t u8_Lowest; /**< Low byte */
    uint8_t u8_Low; /**< Low byte */
    uint8_t u8_High; /**< High byte */
    uint8_t u8_Highest; /**< High byte */
#else
    uint8_t u8_Highest; /**< High byte */
    uint8_t u8_High; /**< High byte */
    uint8_t u8_Low; /**< Low byte */
    uint8_t u8_Lowest; /**< Low byte */
#endif
  } st; /**< Structure */
  st_Bitfield32_t st_Bitfield32; /**< @ref st_Bitfield32_t */
} un_Int32_t;


/**
 * Software version information
 <pre>
 u4_Major
 |  u4_Minor
 |  |  u8_Patch
 |  |  |     u16_Revision
 |  |  |     |           u8_CRC_Value
 |  |  |     |           |
 |  |  |     |           |     PartNumber (6 Nibble)
 |  .  |  .  |  .  |  .  |  .  |  .  |  .  |  .  |
 -------------------------------------------------
 byte   0     1     2     3     4     5     6     7     8
 </pre>
 */
typedef struct
{
  uint16_t u8_Patch :8; /**< Patch */
  uint16_t u4_Minor :4; /**< Minor version */
  uint16_t u4_Major :4; /**< Major version */
  uint16_t u16_Revision; /**< Revision */
  uint8_t u8_CRC_Value; /**< CRC */
  uint8_t u4_PartNumber1 :4; /**< Material number 1 */
  uint8_t u4_PartNumber0 :4;/**< Material number 0 */
  uint8_t u4_PartNumber3 :4;/**< Material number 3 */
  uint8_t u4_PartNumber2 :4;/**< Material number 2 */
  uint8_t u4_PartNumber5 :4;/**< Material number 5 */
  uint8_t u4_PartNumber4 :4;/**< Material number 4 */
} st_SoftwareInfo_t;

/* public variables */
#ifdef _COSMIC_ /* if compiled with CosmicCompiler this is activated */
extern const st_SoftwareInfo_t st_SoftwareInfo @SOFTWARE_INFO_BASE_ADDRESS; /**< Software information structure */
#else
extern const st_SoftwareInfo_t st_SoftwareInfo; /**< Software information structure */
#endif /* __CSMC__ */
/* Public functions ======================================================= */

extern void CFG_init( void );

#endif /* #ifndef _CFG_H_ */

/* end of CFG.h =========================================================== */

