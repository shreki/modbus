/** @brief  Allgemeingueltige Definitionen
*   @file   appl_def.h  
*
*   @author Markus Teubner
*   @date   22.08.2011
*/

#ifndef __appl_def_h__
#define __appl_def_h__

/* Allgemeine Definitionen  */

/** @brief Enum fuer Stati
*
*	neu 27.09.2012
*
*/
typedef enum
{
	OFF,
	ON
} enm_CC_STATE;

/** @brief Enum fuer Antworten
*
*	z.b. fuer Funktionen, wie "enm_CC_ANSWER cc_is_display_content_changed(void)"
*	neu 27.09.2012
*
*/
typedef enum
{
	NO,
	YES
} enm_CC_ANSWER;


#define    ZU                1
#define    AUF               0

#define    JA                1
#define    NEIN              0

#define    EIN               1
#define    AUS               0

#define    TRUE              1
#define    FALSE             0

#define    NEGATIV           0
#define    POSITIV           1

#define    HIGH              1
#define    LOW               0

#define    SOLL              0
#define    IST               1

#define    FEHLERFREI        0
#define    FEHLER            1
#define    FEHLER1           2
#define    FEHLER2           3

#define    RELAIS_EIN        1
#define    RELAIS_AUS        0

#define    EINGANG           1 
#define    EXIST             0

#define    BIT0              0x01
#define    BIT1              0x02
#define    BIT2              0x04
#define    BIT3              0x08
#define    BIT4              0x10
#define    BIT5              0x20
#define    BIT6              0x40
#define    BIT7              0x80
/** ermittelt den Wert eines Bits innerhalb eines Bytes
*   �bergabewert: Variablenname des Bytes und Bit-Nummer (0..7)
*   Beispiel:   P1_3 = mGETBIT(Varname, BIT3)
*   P1.3 nimmt den Wert des Bits 3 der Byte-Variablen "Varname" an.
*/
#define   mGETBIT(VAR_NAME, BIT_NR)     (((VAR_NAME) & (BIT_NR)) != 0)

/* Wochentage */
#define    MONTAG      1
#define    DIENSTAG    2
#define    MITTWOCH    3
#define    DONNERSTAG  4
#define    FREITAG     5
#define    SAMSTAG     6
#define    SONNTAG     7
/* 8 ist belegt, f�r keine SCHALTZEIT */
#define    MO_SO       9 
#define    MO_FR      10 
#define    MO_DO      11 
#define    SA_SO      12 

/* Definitionen fuer den Heizkreisstatus */
#define    SOFORT_AUS        0
#define    ABSENKBETRIEB     1
#define    NORMALBETRIEB     2
#define    I_SCHALTZEIT      2
#define    II_SCHALTZEIT     3
#define    III_SCHALTZEIT    4
#define    KUEHLBETRIEB      5
#define    IV_SCHALTZEIT     6
#define    V_SCHALTZEIT      7
#define    VI_SCHALTZEIT     8

/* Geraetekonfiguration */
#define    KONFIG_SPEICHER      0x0001
#define    KONFIG_BRENNER2      0x0002
#define    KONFIG_KM1           0x0004
#define    KONFIG_COCO          0x0008
#define    KONFIG_E6            0x0010
#define    KONFIG_DCF           0x0020
#define    KONFIG_KESSEL        0x0040
#define    KONFIG_MISCHER1      0x0080
#define    KONFIG_MISCHER2      0x0100
#define    KONFIG_MM1           0x0200
#define    KONFIG_BM            0x0400
#define    KONFIG_MANAGER       0x0800
#define    KONFIG_HEIZMODUL     0x1000
#define    KONFIG_SOLAR         0x2000 
#define    KONFIG_WP            0x4000

/* Definitionen fuer die Modultypenkennungen im Datenbyte 0 */
#define cDIRECT                 0x00    /* Nur RS232 */
#define cKESSEL                 0x03
#define cSPEICHERMODUL          0x04
#define cATEZ                   0x05    /* Alternativer Energieerzeuger */
#define cBEDIEN                 0x06
#define cIO_MODUL               0x07
#define cRAUM_FERNSTELLER       0x08    /* eine Art FBR �ber Bus */
#define cMANAGER                0x09
#define cHEIZMODUL              0x0a
#define cALLE_MODULE            0x0b
#define cMISCHER                0x0c
#define cHZK_MODUL              0x0c
#define cPC                     0x0d
#define cFREMDGERAET            0x0e
#define cZEITMODUL              0x0f

/* Definition fuer die Funktion der Heizkreise */
#define STANDARD_HZK        0   /* Standardfunktion wie E6 */
#define FESTE_VL_HZK        1   /* Regelung auf feste VL-Temp (VL-min / VL-max ) */
#define SCHWIMMBAD_HZK      2   /* Kromschroeder Schwimmbadregelung mit Mischer */
#define WARMWASSER_HZK      3   /* Warmwasserkreis */
#define RUECKLAUF_HZK       4   /* R�cklaufanhebung mit Mischer */
#define SAMMLER_HZK         5   /* Bisher nur im 4401 */

/* Definitionen fuer den Geraete Subtype fuer Parametriersoftware */
#define    GERAET_E6        0
#define    GERAET_BM        1
#define    GERAET_KKM       2
#define    GERAET_WPM       3
#define    GERAET_MM1       4
#define    GERAET_KM1       5
#define    GERAET_IWS       6
#define    GERAET_MODKESSEL 7

/* Fuer die Schieberegisterfkt. */
#define    SOFORT           1
#define    ZYKLISCH         0

/* Programmschalterstellung */
#define    PS_SERVICE               0
#define    PS_BEREIT                1
#define    PS_UHR                   2
#define    PS_SONNE                 3
#define    PS_MOND                  4
#define    PS_WASSER                5
#define    PS_SERVICE_MISCHER_AUF   6
#define    PS_SERVICE_MISCHER_ZU    7
#define    PS_SERVICE_II            9
#define    PS_WASSER_BEREIT        10
#define    PS_UHR1                 11
#define    PS_UHR2                 12
#define    PS_UHR3                 13
#define    PS_HAND                 14
#define    PS_PARTY                15
#define    PS_FERIEN               16
#define    PS_KUEHLEN              17   /* Heizkreis fuehrt Waerme in den Ruecklauf ab, Mischer arbeiten invertiert */
#define    PS_ECO                  20

/* Definitionen f�r den Leistungszwang */
#define KEIN_LEISTUNGSZWANG_ALT           -1    /* F�r alte BMs (uint8)0xFF <-> neue (int8)-1 */
#define KEIN_LEISTUNGSZWANG                0    /* Achtung, dieser Wert wird von K4-Reglern als LEISTUNGSZWANG_SOFORT_AUS interpretiert und darf daher nicht verwendet werden! */
#define LEISTUNGSZWANG_HK_SOFORT_AUS    -100    /* Heizkreispumpe wird ausgeschaltet Vorlaufsolltemp auf 0�C geregelt */
#define LEISTUNGSZWANG_VL_MIN            -99    /* Heizkreispumpe bleibt eingeschaltet (wird nicht aktiv eingeschaltet), Regelung auf Vorlaufmin */
/* Dazwischen werden prozentual die relativen Werte zu minimaler (negativ) oder maximaler (positiv) Vorlauftemperatur geschickt */
/* Negative Werte k�nnen einen Heizkreis nur abschalten, positive nur einschalten */
#define LEISTUNGSZWANG_VL_MAX            100    /* Heizkreispumpe wird/bleibt eingeschaltet, der Heizkreis wird auf Vorlaufmaxtemp geregelt */

/* Konstanten fuer Fuehlerfehler */
#define    KEINFUEHLER       -400
#define    SCHLUSS           -500
#define    BRUCH             -600

/* Datenschluessel Telegramm Fehlermeldungen */
#define FEHLERKENNUNG_INFO              'I'
#define FEHLERKENNUNG_WARNUNG           'W'
#define FEHLERKENNUNG_FEHLER            'E'

/* OSNABRUECKER FEHLERNUMMERN */
#define FEHLER_TB_UEBERTEMPERATUR               1
#define FEHLER_GAS_PRESSURE_SWITCH              2
#define FEHLER_KEINE_FLAMMENBILDUNG             4
#define FEHLER_TW_UEBERTEMPERATUR               6
#define FEHLER_TBA_UEBERTEMPERATUR              7
#define FEHLER_VORLAUFFUEHLER_DEFEKT            12
#define FEHLER_SPEICHERFUEHLER_DEFEKT           14
#define FEHLER_AUSSENFUEHLER_DEFEKT             15
#define FEHLER_RUECKLAUFFUEHLER_DEFEKT          16
#define FEHLER_FEHLER_STROEMUNGSUEBERWACHUNG    40
#define FEHLER_STROEMUNGSUEBERWACHUNG           41
/* ENDE OSNABRUECKER FEHLERNUMMERN */

/* Max Fehlernummer der Osnabruecker */
#define MAX_KMOS_FEHLERNUMMER           50 
#define FEHLER_WARTUNG                  51

#define SPEICHERLADEZEIT_UEBERSCHRITTEN 52
#define OELSTANDGEBER_FEHLER            53

#define FEHLER_WP_STOERUNG              54
#define INFO_WP_SPERRE                  55

#define FEHLER_STOERUNG_STB             60
#define FEHLER_STOERUNG_BRENNER         61
#define FEHLER_SOLARPUMPE               62     
#define FEHELR_SOLARKLAPPE              63      /* z.B. Sch�co: Uhrzeit zwischen 1:00-4:00, Tkol>45�C und Tkol>Tsp */
#define FEHLER_SOLARPUMPE_PLAUSI        64      /* z.B. Sch�co: Keine Impulse f�r Volumenstrom */
#define FEHLER_SOLARPUMPE2_PLAUSI       65      /* z.B. Sch�co: Keine Impulse f�r Volumenstrom */

#define FEHLER_KOLLEKTORFUEHLER2        67
#define FEHLER_KOLLEKTORFUEHLER         68
#define FEHLER_VORLAUFFUEHLER2          69
#define FEHLER_VORLAUFFUEHLER           70
#define FEHLER_FUEHLER_F1               71
#define FEHLER_FUEHLER_F3               72
#define FEHLER_FUEHLER_F4               73
#define FEHLER_WASSERDRUCK              74
#define FEHLER_AUSSENFUEHLER            75
#define FEHLER_SPEICHERFUEHLER          76
#define FEHLER_KESSELFUEHLER            77
#define FEHLER_SAMMLERFUEHLER           78
#define FEHLER_MULTIFUNKTIONSFUEHLER    79
#define FEHLER_FUEHLER_F2               79
#define FEHLER_EXT_RAUMFUEHLER          80
#define FEHLER_EEPROM                   81
#define FEHLER_OELSTAND                 82
#define FEHLER_EXT_RAUMFUEHLER2         83
#define FEHLER_FEUCHTEFUEHLER           84
#define FEHLER_LOW_BATTERY              85

#define FEHLER_KUNDEN_EEPROM            88
#define FEHLER_KUNDENKENNUNG            89
#define FEHLER_BM0_UND_BM1_AM_BUS       90
#define FEHLER_BUSKENNUNG_BELEGT        91
#define FEHLER_KOMMUNIKATION            92
#define FEHLER_COMBUFFER_UEBERLAUF      93
#define FEHLER_TRS_BIT                  94
#define FEHLER_INTERRUPT                95
#define FEHLER_BUS_STROM                96
#define FEHLER_RUECKLAUF                97

#define FEHLER_HARDWARE                 99 /* Osnabruecker Systemfehler */
#define FEHLER_FUEHLER_ABGAS            100

#define FEHLER_FUEHLER_F5               128
#define FEHLER_FUEHLER_F6               129
#define FEHLER_FUEHLER_F7               130
#define FEHLER_FUEHLER_F8               131
#define FEHLER_FUEHLER_F9               132
#define FEHLER_FUEHLER_F10              133
#define FEHLER_FUEHLER_F11              134
#define FEHLER_FUEHLER_F12              135
#define FEHLER_FUEHLER_F13              136
#define FEHLER_FUEHLER_F14              137
#define FEHLER_FUEHLER_F15              138
#define FEHLER_FUEHLER_F16              139
#define FEHLER_FUEHLER_F17              140
#define FEHLER_FUEHLER_F18              141
#define FEHLER_FUEHLER_F19              142
#define FEHLER_FUEHLER_F20              143

#define FEHLER_VOLUMENSTROM_2_ZU_GERING 196
#define FEHLER_VOLUMENSTROM_2_ZU_HOCH   197 
#define FEHLER_VOLUMENSTROM_ZU_GERING   198     
#define FEHLER_VOLUMENSTROM_ZU_HOCH     199     

/* Fehlermeldung, wenn ein Heizmodul nicht mehr am Bus gefunden wird */
#define FEHLER_BUS_HEIZMODUL_0          200
#define FEHLER_BUS_HEIZMODUL_1          201
#define FEHLER_BUS_HEIZMODUL_2          202
#define FEHLER_BUS_HEIZMODUL_3          203
#define FEHLER_BUS_HEIZMODUL_4          204
#define FEHLER_BUS_HEIZMODUL_5          205
#define FEHLER_BUS_HEIZMODUL_6          206
#define FEHLER_BUS_HEIZMODUL_7          207
#define FEHLER_BUS_HEIZMODUL_8          208
#define FEHLER_BUS_HEIZMODUL_9          209
#define FEHLER_BUS_HEIZMODUL_10         210
#define FEHLER_BUS_HEIZMODUL_11         211
#define FEHLER_BUS_HEIZMODUL_12         212
#define FEHLER_BUS_HEIZMODUL_13         213
#define FEHLER_BUS_HEIZMODUL_14         214
#define FEHLER_BUS_HEIZMODUL_15         215

/* Fehlermeldung, wenn ein Bedienmodul nicht mehr am Bus gefunden wird */
#define FEHLER_BUS_BM_0                 220
#define FEHLER_BUS_BM_1                 221
#define FEHLER_BUS_BM_2                 222
#define FEHLER_BUS_BM_3                 223
#define FEHLER_BUS_BM_4                 224
#define FEHLER_BUS_BM_5                 225
#define FEHLER_BUS_BM_6                 226
#define FEHLER_BUS_BM_7                 227
#define FEHLER_BUS_BM_8                 228
#define FEHLER_BUS_BM_9                 229
#define FEHLER_BUS_BM_10                230
#define FEHLER_BUS_BM_11                231
#define FEHLER_BUS_BM_12                232
#define FEHLER_BUS_BM_13                233
#define FEHLER_BUS_BM_14                234
#define FEHLER_BUS_BM_15                235

/* reserviert #define FEHLER_BUS_MANAGER          240 Fehlermendung wenn der Manager nicht mehr am Bus ist. */
#define FEHLER_BUS_KESSEL   241 /* Fehlermendung wenn der Kessel nicht mehr am Bus ist. */
#define FEHLER_BUS_MISCHER  242 /* Fehlermendung wenn der Mischer nicht mehr am Bus ist. */
#define FEHLER_BUS_ATEZ     243 /* Fehlermendung wenn der ATEZ nicht mehr am Bus ist. */

/** Definitionen f�r das neue Fehlermanagement
*   Benutzt werden die Infonummern cFEHLERNUMMER und cFEHLERART
*
*   Nach Definition sind OEM-Fehlernummer im Bereich 800...999 und 8000...9999 untergebracht.
*   Die Fehlernummern sind dabei nur innerhalb eines OEM-Kunden eindeutig.
*   Allgemein g�ltige Fehlernummern werden in den Bereichen 500... 799 und 1000...7999 kodiert.
*   Der Bereich 256...499 ist f�r die Kollegen in OS reserviert.
*/

#define FEHLER_VERFLUESSIGERFUEHLER 501         /* wird z.Z. genutzt um einen Bruch/Kurzschluss des F�hlers f�r die Verfl�ssigertemperatur einer W�rmepumpe zu signalisieren */

#endif // __appl_def_h__
