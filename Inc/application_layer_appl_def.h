/** @brief Header fuer applikationsspezifische Defintionen des ModBus
*   @file application_layer_appl_def.h
*
*   beinhaltet applikationsspezifische Defintionen fuer den ModBus Treiber
*
*   @author Volker Hildebrandt
*   @date   08.05.2012
*   @date   Letzte Aenderung: 08.05.2012 - Hildebrandt
*/


#ifndef __application_layer_appl_def_h__
#define __application_layer_appl_def_h__

												                  
				
#ifdef PC_VERSION		
	#define		NUM_MOD_BUS_DATA_BYTES		252		/** maximale Anzahl Datenbytes innerhalb eines ModBus Telegrammframes.
												                  *	Der lt. ModBus Spezifikation maximal mögliche Wert ist 252 !!! */
  #ifdef STE_INVERTER										                  
    #define   PC_COM_PORT         6
    #define   PC_BAUDRATE         MOD_BUS_BAUD_19200
    #define   PC_PARITY           MOD_BUS_EVEN_PARITY
    #define   MOD_BUS_SLAVE_ADR   (uINT8) 45
    #define   STE_INV_FUNC_READ   (uINT8) 3
    #define   STE_INV_FUNC_WRITE  (uINT8) 6
  #endif
#elif uPC70F3619   || BSP_USE_WINDOWS == 1  || BSP_USE_STM32 == 1
    #define     MOD_BUS_ERR_DELAY           5                   /* Verzögerung vor Senden des 1. erkannten Fehlers eines Fehlerobjekts [s] */

	#define		NUM_MOD_BUS_DATA_BYTES		4					/* 2 Byte Regsiteradresse + 2 Byte Anzahl Datenbytes */
	#define 	MOD_BUS_BAUD_RATE			MOD_BUS_BAUD_19200	
	#define 	MOD_BUS_PARITY				MOD_BUS_EVEN_PARITY
	#define 	INV_SLAVE_ADR				45
	#define   	INV_FUNC_READ   		    3
	#define   	INV_FUNC_WRITE   		    6
	#define 	MOD_BUS_REQ_TIMEOUT			500					/* [ms] */
	#define		MOD_BUS_KOMM_TIMEOUT		50					/* [s] */
	#define 	MOD_BUS_ZYKL_INTERVALL_UNCHANGED	5 			/* [s] */	
	#define 	MOD_BUS_ZYKL_INTERVALL_CHANGED	2 				/* [s] */	
	
	#define		MOD_ERR_TIME_COUNTER		(8 * MOD_BUS_ZYKL_INTERVALL_UNCHANGED)

	#define 	INV_HOCHLAUFSTATUS			KP_1_0
	#define 	INV_VERD_DREHMOMENT			KP_1_2
	#define 	INV_NETZSPANNUNG			KP_1_3
	#define 	INV_NETZSTROM				KP_1_4
	#define 	INV_NETZLEISTUNG			KP_1_5
	#define 	INV_MOTORSTROM				KP_1_6
	#define 	INV_MOTORTEMP				KP_1_7
	#define 	INV_SCROLLTEMP				KP_1_8

	#define 	INV_HAUPTFEHLER_1_GEH		KP_1_9
	#define 	INV_NEBENFEHLER_1_GEH		KP_1_10
	#define 	INV_HAUPTFEHLER_1			KP_1_11
	#define 	INV_NEBENFEHLER_1			KP_1_12

	#define 	INV_HAUPTFEHLER_2_GEH		KP_1_13
	#define 	INV_NEBENFEHLER_2_GEH		KP_1_14
	#define 	INV_HAUPTFEHLER_2			KP_1_15
	#define 	INV_NEBENFEHLER_2			KP_1_16
	
	
	#define 	INV_VERD_IST_DREHZAHL		KP_1_1
	#define		INV_HAUPTFEHLER_1_GEHALTEN	KP_1_9	
	#define		INV_NEBENFEHLER_1_GEHALTEN	KP_1_10
	#define		INV_HAUPTFEHLER_1			KP_1_11
	#define		INV_NEBENFEHLER_1			KP_1_12
	#define		INV_HAUPTFEHLER_2_GEHALTEN	KP_1_13
	#define		INV_NEBENFEHLER_2_GEHALTEN	KP_1_14
	#define		INV_HAUPTFEHLER_2			KP_1_15
	#define		INV_NEBENFEHLER_2			KP_1_16	
	
	#define		INV_INVERTER_FREIGABE		KP_1_17
	#define		INV_INVERTER_SOLL_DREHZAHL	KP_1_18	
	#define		INV_INVERTER_RESET			KP_1_19
	
	#define		INV_DREHMOMENT_GRENZE		KP_2_0
	#define		INV_SPG_ABWEICHUNG			KP_2_1
	#define		INV_SPG_ZWISCHENKREIS		KP_2_2
	#define		INV_SCHALTKREIS_TEMP		KP_2_3
	#define		INV_PHASE_A_TEMP			KP_2_4
	#define		INV_PHASE_B_TEMP			KP_2_5
	#define		INV_PHASE_C_TEMP			KP_2_6
	#define		INV_PFC_PHASE_A_TEMP		KP_2_7
	#define		INV_PFC_PHASE_B_TEMP		KP_2_8
	#define		INV_PFC_PHASE_C_TEMP		KP_2_9
    
    #define     INV_KONF_ZUGR_PWD           KP_4_1    
    #define     INV_KONF_MOD_NR             KP_4_2    
    #define     INV_KONF_EIGENSCH_SENSOR_1        KP_4_3    
    #define     INV_KONF_VD_DRZ_RAMPE_HOCHL       KP_4_4    
    #define     INV_KONF_VD_DRZ_RAMPE_RUNTERL     KP_4_5    
    #define     INV_KONF_VD_DRZ_RAMPE_ABSCHALT    KP_4_6    
    #define     INV_KONF_EIGENSCH_SENSOR_2        KP_4_7    
#endif  

/* Prototypen */
void convert_fahrenheit_celsius(FLP32* temp_wert_pflp32);
void convert_drehzahl_einheit(FLP32* umdrehungen_wert_pflp32);
void modbus_kommu_manager_50_ms_aktionen( void );
void modbus_fehler_manager_50_ms_aktionen( void );
void modbus_set_obj_timout_stop(uINT8 neuer_wert_u8);
uint16 mod_bus_kommu_manager_get_dbg_state( void );
uint16 mod_bus_appl_layer_get_dbg_state( void );
uINT8 mod_bus_get_kommu_state( void );

void modbus_hochlauf_status_komm_callback(FLP32* daten_wert_flp32);
void modbus_hauptfehler_1_gehalten_komm_callback(FLP32* daten_wert_flp32);
void modbus_hauptfehler_1_komm_callback(FLP32* daten_wert_flp32);
void modbus_nebenfehler_1_gehalten_komm_callback(FLP32* daten_wert_flp32);
void modbus_nebenfehler_1_komm_callback(FLP32* daten_wert_flp32);
void modbus_hauptfehler_2_gehalten_komm_callback(FLP32* daten_wert_flp32);
void modbus_hauptfehler_2_komm_callback(FLP32* daten_wert_flp32);
void modbus_nebenfehler_2_gehalten_komm_callback(FLP32* daten_wert_flp32);
void modbus_nebenfehler_2_komm_callback(FLP32* daten_wert_flp32);


#ifdef SIM_INV_FEHLER
void set_sim_inv_hauptfehler1_gehalten( FLP32* reg_wert_pflp32 );
void set_sim_inv_hauptfehler2_gehalten( FLP32* reg_wert_pflp32 );
void set_sim_inv_nebenfehler1_gehalten( FLP32* reg_wert_pflp32 );
void set_sim_inv_nebenfehler2_gehalten( FLP32* reg_wert_pflp32 );
void set_sim_inv_hauptfehler1( FLP32* reg_wert_pflp32 );
void set_sim_inv_hauptfehler2( FLP32* reg_wert_pflp32 );
void set_sim_inv_nebenfehler1( FLP32* reg_wert_pflp32 );
void set_sim_inv_nebenfehler2( FLP32* reg_wert_pflp32 );
#endif

#endif
