


/** @brief Header des Application Layer
*   @file application_layer_main_pc.h
*
*   PC Implementierung des Application Layers
*	
*
*   @author Volker Hildebrandt
*   @date   14.05.2012
*   @date   Letzte Aenderung: 14.05.2012 - Hildebrandt
*/

#define NUM_STE_INVERTER_MOD_OBJ      43

// Defines f�r bitcodierte Zust�nde des ModBus Applikationslayers
// Ein gesetztes Bit bedeutet, dass die zugeh�rige Stelle im Source durchlaufen wurde
#define MOD_BUS_DBG_MAIN_LOOP_ENTRY			0x0001
#define MOD_BUS_DBG_MAIN_LOOP_EXIT			0x0002
#define MOD_BUS_DBG_START_MOD_BUS			0x0004


typedef struct
{
  /** Adresse des auszulesenden Registers */
  uINT16  adr_u16;
  /** Name des auszulesenden Registers */
  sINT8   register_name_s8[1];   
  /** Skalierungsfaktor des Wertes */
  uINT16  scale_u16;  
} STE_Inverter_ModBus_Read_Obj_strct_def;


// Prototypen
// aus 'modbus_fehlermanagment.c'
void chk_inverter_fehler( void ); 
void chk_inverter_reset( void );	
void chk_ueberwachung_inv_fehlerreset( void );
uINT8 modbus_chk_inverter_hochlauf_status( void );