/** @brief Header fuer allg. Data Link Layer Funktionen
*   @file data_link_layer_common.h
*
*   beinhaltet Definitionen und Prototypen der Schnittstelle
*   zum Application Layer sowie 
*   zur hardwarespezifischen Data Link Layer Schicht
*	Diese Datei sollte nur durch die Data Link Layer Schicht
* 	includiert werden
*
*   @author Volker Hildebrandt
*   @date   08.05.2012
*   @date   Letzte Aenderung: 08.05.2012 - Hildebrandt
*/

#ifndef __data_link_layer_common_h__
#define __data_link_layer_common_h__

#include "application_layer_appl_def.h"

typedef sINT8 (*wait_comm_semaphore_func) (uINT32);		
typedef void  (*release_comm_semaphore_func) (void);

typedef struct
{
  /** Adresse des ModBus Slaves */
	uINT8	slave_adr_u8;
	/** Funktionscode des ModBus Frames */
	uINT8	func_code_u8;
	/** eigenliche Datenbytes eines Frames */
	uINT8 	data_u8[NUM_MOD_BUS_DATA_BYTES];         	  
	/** Timeout des Requests [ms]*/
	uINT16	req_timeout_u16;								                
	/** Anzahl Datenbytes im aktuellen Frame */
	uINT16	num_write_data_bytes_u16;                   
	/** Anzahl Datenbytes im Antwort Frame (0 -> Anzahl Datenbytes wird aus Antwort Frame ermittelt)*/
	uINT16	num_read_data_bytes_u16;                    
	/** ggfs. Fehlercode des letzten ModBus Zugriffs */	
	sINT16	error_code;									                
	/** Aktueller Zustand der Kommunikations Zustandsmaschine */
	mod_bus_comm_state_enm	state_enm;					        
	/** Funktionszeiger Wartefunktion Semaphore. Nur mit PC Software verwenden. Auf dem Target '0' setzen */
	wait_comm_semaphore_func	wait_comm_semaphore; 	    
	/** Funktionszeiger Releasefunktion Semaphore. Nur mit PC Software verwenden. Auf dem Target '0' setzen */
	release_comm_semaphore_func release_comm_semaphore; 
} mod_bus_data_def_strct;

uINT16 calc_CRC16 ( mod_bus_data_def_strct* mod_bus_data_pstrct, uINT16 num_bytes_u16);
sINT8 send_mod_bus_data(mod_bus_data_def_strct* mod_bus_data_pstrct);
void eval_read_bytes(mod_bus_data_def_strct* mod_bus_data_pstrct, uINT8* read_data_u8, uINT8* num_read_bytes_pu8, uINT8 bytes_read_u8);

#endif
