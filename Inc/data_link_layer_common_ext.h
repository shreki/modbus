/** @brief Header fuer externe Data Link Layer Funktionen
*   @file data_link_layer_common_ext.h
*
*   beinhaltet Definitionen und Prototypen der Schnittstelle
*   zum Application Layer sowie 
*   zur hardwarespezifischen Data Link Layer Schicht
*	Diese Datei dient zur Bekanntmachung der Schnittstelle
*  im Application Layer
*
*   @author Volker Hildebrandt
*   @date   08.05.2012
*   @date   Letzte Aenderung: 08.05.2012 - Hildebrandt
*/


#ifndef __data_link_layer_common_ext_h__
#define __data_link_layer_common_ext_h__

#define	MOD_BUS_OK					0
#define MOD_BUS_REQ_IN_PROGRESS		1
// ab hier Fehlerstati
#define MOD_BUS_INIT_FAIL			-1      /* ModBus Initstialisierung ist fehlgeschlagen */   
#define MOD_BUS_BUSY_ERR			-2		/* ModBus Lese/Schreibauftrag kann nicht gestartet werden, vorheriger Auftrag wird noch abgearbeitet */ 
#define MOD_BUS_THREAD_FAIL			-3      /* NUR PC Version: Thread f�r ModBus Status Maschine kann nicht gestartet werden */
#define MOD_BUS_PARAM_ERR			-4		/* ung�ltiger Parameterwert beim Aufruf der ModBus Initialisierungsfunktion */
#define MOD_BUS_NO_INIT				-5		/* Lese-/Schreibanfrage vor Ausf�hren der ModBus Initialisierungsfunktion */
#define MOD_BUS_INVALID_SLAVE		-6		/* ModBus Slave Adresse in Antwort stimmt nicht geschriebener Slave Adresse ueberein */
#define MOD_BUS_CRC_ERR				-7		/* CRC16 Fehler in Slave Antwort */
#define	MOD_BUS_UART_WRITE_ERR		-8		/* Fehler beim Schreiben auf den UART */
#define MOD_BUS_TIMEOUT				-9		/* Timeout beim Warten auf Antwort vom Slave */
#define	MOD_BUS_UART_READ_ERR		-10		/* Fehler beim Lesen vom UART */
#define	MOD_BUS_INTERBYTE_ERR		-11		/* Zeit zwischen 2 Bytes im ModBus Frame zu gross (Timer t1.5 abgelaufen) */
#define MOD_BUS_OVERRUN   			-12     /* Neue Anforderung bevor die Antwort der letzten abgeholt wurde */   
#define MOD_BUS_ERR_FLAG			-13		/* in der Antwort des Slave ist das MSB im Funktionscode gesetzt: Slave signalisiert Fehler */    


typedef enum
{	
  MOD_BUS_DEFAULT_BAUD,
	MOD_BUS_BAUD_9600,
	MOD_BUS_BAUD_19200
} baudrate_def_enm;

typedef enum
{
	MOD_BUS_NO_PARITY,
	MOD_BUS_EVEN_PARITY,
	MOD_BUS_ODD_PARITY
} parity_def_enm;


typedef enum
{
	MOD_BUS_COMM_NO_INIT,								/** ModBus Init ist nicht durchgef�hrt */	
	MOD_BUS_COMM_IDLE,                                  /** ModBus ist frei */
	MOD_BUS_COMM_REQ_SET,     		                    /** Neue Schreibdaten wurden zugewiesen */	
	MOD_BUS_COMM_IN_PROGRESS,                     		/** Bearbeitung eines Request laeuft */	
	MOD_BUS_COMM_WRITE_ERROR,							/** Daten schreiben mit Fehler abgeschlossen */
	MOD_BUS_COMM_READ_ERROR,                            /** Daten lesen mit Fehler abgschlossen */	
	MOD_BUS_COMM_REQ_TIMEOUT,							/** Timeout bei Bearbeitung des Requests */
	MOD_BUS_COMM_DATA_VALID	                    		/** Kommunikation erfolgreich. daten k�nnen gelesen werden */
} mod_bus_comm_state_enm;

typedef enum
{
	MOB_BUS_READ_REQ,									/** ModBus Lesezugriff */
	MOB_BUS_READ_WRITE									/** ModBus Schreibzugriff */		
} mod_bus_req_type_def_enm;



sINT8 init_mod_bus(baudrate_def_enm baudrate_enm, parity_def_enm parity_enm, uINT16 interbyte_timeout_u16, uINT16 frame_pause_u16);

sINT8 req_modbus_data(uINT8 slave_adr_u8,
                      uINT8 func_code_u8,
                      uINT8 num_write_bytes_u8,
                      uINT8 num_read_bytes_u8,
                      uINT8* data_pu8,
                      uINT16 req_timeout_u16);
                      
sINT16 chk_modbus_data_req(uINT8* func_code_pu8, uINT16* num_read_bytes_pu16, uINT8* read_data_pu8);


#ifndef PC_VERSION
void mod_bus_status_maschine( void );
void modbus_100_ms_aktionen( void );
uint16 mod_bus_data_link_layer_get_dbg_state( void );
#endif

void close_mod_bus( void );

#endif
