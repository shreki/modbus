/** @brief  Prozessorabh�ngige Definitionen f�r allgemeing�ltige Datentypen
*   @file   datatypes.h  
*
*   @author Dennis Habener
*   @date   21.10.2010
*/

#ifndef __datatypes_h__
#define __datatypes_h__

/**
*   alte Datentypen zwecks Abw�rtskompatibilit�t
*/
typedef unsigned char   bit_var;    /**< Fuer evtl. moegliche Bitoperationen */
typedef signed char     int8;       /**< signed Char */
typedef signed char     int8_t;       /**< signed Char */
typedef signed short    int16;      /**< signed Short */
typedef signed short    int16_t;      /**< signed Short */
typedef signed long     int32;      /**< signed Long */
typedef signed long     int32_t;      /**< signed Long */
typedef unsigned char   uint8;      /**< unsigned Char */
typedef unsigned char   uint8_t;      /**< unsigned Char */
typedef unsigned short  uint16;     /**< unsigned Short */
typedef unsigned short  uint16_t;     /**< unsigned Short */
typedef unsigned long   uint32;     /**< unsigned Long */
typedef unsigned long   uint32_t;
typedef float           float32;    /**< V850: +-1,18E-38 to +-3,39E+38 */
typedef char            achar;      /**< Fuer Zeichenausgabe �ber ASCII - Sollte k�nftig nicht verwendet werden, da keine eindeutige Vorzeichenkennung */
typedef enum  {FALSE = 0, TRUE = 1} bool;    /**< unsigned char */



/* Bei 78K sind die Datentypen float double und long double alle 32 Bit */
#ifdef __ICCV850__
    typedef double     float64;     /* V850: +-1,79E-308 to +-1,79E+308 */
#endif

/**
*   Ganzzahlige Werte
*/
typedef signed char     sINT8;          /**< signed Char*/
typedef signed short    sINT16;         /**< signed Short */
typedef signed long     sINT32;         /**< signed Long */
typedef unsigned char   uINT8;          /**< unsigned Char */
typedef unsigned short  uINT16;         /**< unsigned Short */
typedef unsigned long   uINT32;         /**< unsigned Long */
typedef signed char     sint8_t;          /**< signed Char*/
typedef signed short    sint16_t;         /**< signed Short */
typedef signed long     sint32_t;         /**< signed Long */
typedef float           float32_t;
/**
*   Zeiger
*/
typedef void*               pVOID;      /**< Pointer auf Void */
typedef signed char*        STRING;     /**< Pointer auf signed Char */
typedef signed char*        psINT8;     /**< Pointer auf signed Char */
typedef signed short*       psINT16;    /**< Pointer auf signed Short */
typedef signed long*        psINT32;    /**< Pointer auf signed Long */
typedef unsigned char*      puINT8;     /**< Pointer auf unsigned Char */
typedef unsigned short*     puINT16;    /**< Pointer auf unsigned Short */
typedef unsigned long*      puINT32;    /**< Pointer auf unsigned Long */

/**
*   Zeiger auf Zeiger
*/
typedef void**              ppVOID;     /**< Pointer auf Pointer auf Void */
typedef signed char**       pSTRING;    /**< Pointer auf Pointer auf signed Char */
typedef signed char**       ppsINT8;    /**< Pointer auf Pointer auf signed Char */
typedef signed short**      ppsINT16;   /**< Pointer auf Pointer auf signed Short */
typedef signed long**       ppsINT32;   /**< Pointer auf Pointer auf signed Long */
typedef unsigned char**     ppuINT8;    /**< Pointer auf Pointer auf unsigned Char */
typedef unsigned short**    ppuINT16;   /**< Pointer auf Pointer auf unsigned Short */
typedef unsigned long**     ppuINT32;   /**< Pointer auf Pointer auf unsigned Long */

/* Gleitkomma Zahlen */
typedef float           FLP32;      /**< V850: +-1,18E-38 to +-3,39E+38 */
#ifdef __ICCV850__
    typedef double      FLP64;      /**< V850: +-1,79E-308 to +-1,79E+308 */
#endif

#endif // __datatypes_h__
