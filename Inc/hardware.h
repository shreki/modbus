

#define ANZAHL_RELAIS   9
//                                                                                                         V850 Port PIN       Name                        Stecker    V850/STM32
#define VERDICHTER_ND_RELAIS_LESEN      BSP_getDigitalOutputState (BSP_DIGITAL_OUTPUT_1_1)                 //( PDL & 0x0001 )     Verdichter_Nr. 1         ST1.3       DO_1_1
#define VERDICHTER_HD_RELAIS_LESEN      BSP_getDigitalOutputState (BSP_DIGITAL_OUTPUT_1_2)                 //( PDL & 0x0002 )     Verdichter NR. 2         ST1.4       DO_1_2
#define DRUCKAUSGLEICH_RELAIS_LESEN     BSP_getDigitalOutputState (BSP_DIGITAL_OUTPUT_1_3)                 //( PDL & 0x0004 )     Druckausgleich           ST1.5       DO_1_3
#define OELAUSGLEICH_RELAIS_LESEN       BSP_getDigitalOutputState (BSP_DIGITAL_OUTPUT_1_4)                 //( PDL & 0x0008 )     Oelausgleich             ST1.6       DO_1_4
#define ABTAURELAIS_LESEN               BSP_getDigitalOutputState (BSP_DIGITAL_OUTPUT_1_5)                 //( PDL & 0x0010 )     ABTAURELAIS              ST1.7       DO_1_5
#define VERDICHTER_HD_VENTIL_LESEN      BSP_getDigitalOutputState (BSP_DIGITAL_OUTPUT_1_6)                 //( PDL & 0x0020 )     PumpenRELAIS             ST1.8       DO_1_6
#define DHC1_RELAIS_LESEN               BSP_getDigitalOutputState (BSP_DIGITAL_OUTPUT_1_7)                 //( PDL & 0x0040 )     DHC1_RELAIS              ST4.1       DO_1_7
#define DHC2_RELAIS_LESEN               BSP_getDigitalOutputState (BSP_DIGITAL_OUTPUT_1_8)                 //( PDL & 0x0080 )     DHC2_RELAIS              ST4.2       DO_1_8
#define OELSUMPF_RELAIS_LESEN           BSP_getDigitalOutputState (BSP_DIGITAL_OUTPUT_1_9)                 //( PCT & 0x40   )     Oelsumpf                 ST4.3       DO_1_9




//  Relais Einschalten                                                                                        V850 Port PIN         Name                     Stecker    V850/STM32
#define VERDICHTER_ND_RELAIS_EIN        BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_1, BSP_ACTOR_ON)      //PDL |= 0x0001       Verdichter_Nr. 1         ST1.3       DO_1_1
#define VERDICHTER_HD_RELAIS_EIN        BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_2, BSP_ACTOR_ON)      //PDL |= 0x0002       Verdichter NR. 2         ST1.4       DO_1_2
#define DRUCKAUSGLEICH_RELAIS_EIN       BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_3, BSP_ACTOR_ON)      //PDL |= 0x0004       Druckausgleich           ST1.5       DO_1_3
#define OELAUSGLEICH_RELAIS_EIN         BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_4, BSP_ACTOR_ON)      //PDL |= 0x0008       Oelausgleich             ST1.6       DO_1_4
#define ABTAURELAIS_EIN                 BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_5, BSP_ACTOR_ON)      //PDL |= 0x0010       ABTAURELAIS              ST1.7       DO_1_5
#define VERDICHTER_HD_VENTIL_RELAIS_EIN BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_6, BSP_ACTOR_ON)      //PDL |= 0x0020       PumpenRELAIS             ST1.8       DO_1_6
#define DHC1_RELAIS_EIN                 BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_7, BSP_ACTOR_ON)      //PDL |= 0x0040       DHC1_RELAIS              ST4.1       DO_1_7
#define DHC2_RELAIS_EIN                 BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_8, BSP_ACTOR_ON)      //PDL |= 0x0080       DHC2_RELAIS              ST4.2       DO_1_8
#define OELSUMPF_RELAIS_EIN             BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_9, BSP_ACTOR_ON)      //PCT |= 0x40         Oelsumpf                 ST4.3       DO_1_9


//  Relais Ausschalten                                                                                         V850 Port PIN         Name                     Stecker    V850/STM32
#define VERDICHTER_ND_RELAIS_AUS        BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_1, BSP_ACTOR_OFF)      //PDL &= ~0x0001      Verdichter_Nr. 1         ST1.3       DO_1_1
#define VERDICHTER_HD_RELAIS_AUS        BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_2, BSP_ACTOR_OFF)      //PDL &= ~0x0002      Verdichter NR. 2         ST1.4       DO_1_2
#define DRUCKAUSGLEICH_RELAIS_AUS       BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_3, BSP_ACTOR_OFF)      //PDL &= ~0x0004      Druckausgleich           ST1.5       DO_1_3
#define OELAUSGLEICH_RELAIS_AUS         BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_4, BSP_ACTOR_OFF)      //PDL &= ~0x0008      Oelausgleich             ST1.6       DO_1_4
#define ABTAURELAIS_AUS                 BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_5, BSP_ACTOR_OFF)      //PDL &= ~0x0010      ABTAURELAIS              ST1.7       DO_1_5
#define VERDICHTER_HD_VENTIL_RELAIS_AUS BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_6, BSP_ACTOR_OFF)      //PDL &= ~0x0020      PumpenRELAIS             ST1.8       DO_1_6
#define DHC1_RELAIS_AUS                 BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_7, BSP_ACTOR_OFF)      //PDL &= ~0x0040      DHC1_RELAIS              ST4.1       DO_1_7
#define DHC2_RELAIS_AUS                 BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_8, BSP_ACTOR_OFF)      //PDL &= ~0x0080      DHC2_RELAIS              ST4.2       DO_1_8
#define OELSUMPF_RELAIS_AUS             BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_1_9, BSP_ACTOR_OFF)      //PCT &= ~0x40        Oelsumpf                 ST4.3       DO_1_9


// Ansteuerung Schrittmotor EIN     //P3 und P5         Expansionsventil 1
//                                                                                                                                        V850 Port PIN        Name       V850/STM32              Stecker    STM32
#define SM_ND_PHASE_1_EIN               BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_2_1, BSP_ACTOR_ON)	    //P5 |= 0x01           Schrittm. Phase1 1        P5.1       DO_2_1
#define SM_ND_PHASE_2_EIN               BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_2_2, BSP_ACTOR_ON)      //P5 |= 0x02           Schrittm. Phase1 2        P5.2       DO_2_2
#define SM_ND_PHASE_3_EIN               BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_2_3, BSP_ACTOR_ON)      //P3 |= 0x0004         Schrittm. Phase1 3        P5.3       DO_2_3
#define SM_ND_PHASE_4_EIN               BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_2_4, BSP_ACTOR_ON)      //P3 |= 0x0020         Schrittm. Phase1 4        P5.4       DO_2_4

// Ansteuerung Schrittmotor AUS     //P3 und P5         Expansionsventil 1
#define SM_ND_PHASE_1_AUS               BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_2_1, BSP_ACTOR_OFF) 		//P5 &= ~0x01          Schrittm. Phase1 1        P5.1       DO_2_1
#define SM_ND_PHASE_2_AUS               BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_2_2, BSP_ACTOR_OFF)		  //P5 &= ~0x02          Schrittm. Phase1 2        P5.2       DO_2_2
#define SM_ND_PHASE_3_AUS               BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_2_3, BSP_ACTOR_OFF)		  //P3 &= ~0x0004        Schrittm. Phase1 3        P5.3       DO_2_3
#define SM_ND_PHASE_4_AUS               BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_2_4, BSP_ACTOR_OFF)	    //P3 &= ~0x0020        Schrittm. Phase1 4        P5.4       DO_2_4

// Ansteuerung Schrittmotor EIN     //P3 und P5         Expansionsventil 2 f�r Zwischeneinspritzung
#define SM_HD_PHASE_1_EIN           	BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_3_1, BSP_ACTOR_ON)	      //PDL |= 0x0100        Schrittm. EV Phase1 1     P8.1       DO_3_1
#define SM_HD_PHASE_2_EIN           	BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_3_2, BSP_ACTOR_ON)	      //PDL |= 0x0200        Schrittm. EV Phase1 2     P8.2       DO_3_2
#define SM_HD_PHASE_3_EIN           	BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_3_3, BSP_ACTOR_ON)	      //PDL |= 0x0400        Schrittm. EV Phase1 3     P8.3       DO_3_3
#define SM_HD_PHASE_4_EIN           	BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_3_4, BSP_ACTOR_ON)	      //PDL |= 0x0800        Schrittm. EV Phase1 4     P8.4       DO_3_4

// Ansteuerung Schrittmotor AUS     //P3 und P5         Expansionsventil 2 f�r Zwischeneinspritzung
#define SM_HD_PHASE_1_AUS           	BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_3_1, BSP_ACTOR_OFF)	      //PDL &= ~0x0100       Schrittm. EV Phase1 1     P8.1       DO_3_1
#define SM_HD_PHASE_2_AUS           	BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_3_2, BSP_ACTOR_OFF)	      //PDL &= ~0x0200       Schrittm. EV Phase1 1     P8.1       DO_3_2
#define SM_HD_PHASE_3_AUS           	BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_3_3, BSP_ACTOR_OFF)	      //PDL &= ~0x0400       Schrittm. EV Phase1 1     P8.1       DO_3_3
#define SM_HD_PHASE_4_AUS           	BSP_setDigitalOutputState (BSP_DIGITAL_OUTPUT_3_4, BSP_ACTOR_OFF)	      //PDL &= ~0x0800       Schrittm. EV Phase1 1     P8.1       DO_3_4


// Leuchtdioden
#define __TEST_MEASUREMENT_ACTIV__   0
#if  __TEST_MEASUREMENT_ACTIV__  == 1
  /* Refer to STE_TEST.h */
  #define LED_FATAL_AUS
  #define LED_FATAL_EIN
  #define LED_BUS_AUS
  #define LED_BUS_EIN
  #define LED_AUSSENAUFSTELLUNG_AUS
  #define LED_AUSSENAUFSTELLUNG_EIN
#else                                                                                                             //                                                           Stecker                    STM32
  #define LED_FATAL_AUS                   BSP_setLedOutputState (BSP_DIGITAL_OUTPUT_4_1, BSP_ACTOR_ON,  &led_status_u8)     //(P9 |= 0x0001); (led_status_u8 &= ~0x01)     //P9.0 = EIN    --         LEDr
  #define LED_FATAL_EIN                   BSP_setLedOutputState (BSP_DIGITAL_OUTPUT_4_1, BSP_ACTOR_OFF, &led_status_u8)     //(P9 &= ~0x0001); (led_status_u8 |= 0x01)     //P9.0 = AUS    --         LEDr
  #define LED_BUS_AUS                     BSP_setLedOutputState (BSP_DIGITAL_OUTPUT_4_2, BSP_ACTOR_ON,  &led_status_u8)     //(P9 |= 0x0002);  (led_status_u8 &= ~0x02)    //P9.1 = EIN    --         LEDg1
  #define LED_BUS_EIN                     BSP_setLedOutputState (BSP_DIGITAL_OUTPUT_4_2, BSP_ACTOR_OFF, &led_status_u8)     //(P9 &= ~0x0002); (led_status_u8 |= 0x02)     //P9.1 = AUS    --         LEDg1
  #define LED_AUSSENAUFSTELLUNG_AUS       BSP_setLedOutputState (BSP_DIGITAL_OUTPUT_4_3, BSP_ACTOR_ON,  &led_status_u8)     //(P5 |= 0x20);    (led_status_u8 &= ~0x04)    //P5.5 = EIN    --         LEDg2
  #define LED_AUSSENAUFSTELLUNG_EIN       BSP_setLedOutputState (BSP_DIGITAL_OUTPUT_4_3, BSP_ACTOR_OFF, &led_status_u8)     //P5 &= ~0x20;     (led_status_u8 |= 0x04)     //P5.5 = AUS    --         LEDg2
#endif



// RESET-TASTE                                                                                                                                                    STM32
#define RESET_TASTER            	        BSP_getDigitalInputState (BSP_DIGITAL_INPUT_4_9)      //((P9 & 0x2000) == 0)  P9.13 hat GND            RST (Signal)

// DIP-Schalter Maschinentyp (ermitteln der Stellung HEX-Schalter)                                                                                       Stecker    STM32
#define MASCHINENTYP_BIT0                 BSP_getDigitalInputState (BSP_DIGITAL_INPUT_4_1)                       //((P0 & 0x0001) == 0)   //P0.0       --         DIPa1
#define MASCHINENTYP_BIT1                 BSP_getDigitalInputState (BSP_DIGITAL_INPUT_4_2)                       //((P0 & 0x0002) == 0)   //P0.1       --         DIPa2
#define MASCHINENTYP_BIT2                 BSP_getDigitalInputState (BSP_DIGITAL_INPUT_4_3)                       //((P0 & 0x0004) == 0)   //P0.2       --         DIPa3
#define MASCHINENTYP_BIT3                 BSP_getDigitalInputState (BSP_DIGITAL_INPUT_4_4)                       //((P0 & 0x0008) == 0)   //P0.3       --         DIPa4

// DIP-Schalter Betriebsart
#define BETRIEBSART_BIT0                  BSP_getDigitalInputState (BSP_DIGITAL_INPUT_4_5)                       //((P0 & 0x0010) == 0)   //P0.4       --         DIPb1
#define BETRIEBSART_BIT1                  BSP_getDigitalInputState (BSP_DIGITAL_INPUT_4_6)                       //((P0 & 0x0040) == 0)   //P0.6       --         DIPb2
#define BETRIEBSART_BIT2                  BSP_getDigitalInputState (BSP_DIGITAL_INPUT_4_7)                       //((P3 & 0x0100) == 0)   //P3.8       --         DIPb3
#define BETRIEBSART_BIT3                  BSP_getDigitalInputState (BSP_DIGITAL_INPUT_4_8)                       //((P3 & 0x0200) == 0)   //P3.9       --         DIPb4


// Optokoppler-Eing�nge                                                                                                                                             STM32 PIN Belegung
#define HD_STOERUNG_OPTO                  BSP_getDigitalInputState (BSP_DIGITAL_INPUT_1_0)                        //((P7L & 0x01) == 0)                    ST1.9     D1_1_0
#define IMPULSEINGANG_OPTO                BSP_getDigitalInputState (BSP_DIGITAL_INPUT_1_1)                        //((P7L & 0x02) == 0)                    ST1.10    D1_1_1
#define INV_VERSRORGUNG_OPTO		          BSP_getDigitalInputState (BSP_DIGITAL_INPUT_1_1)                    //((P7L & 0x02) == 0)                    ST1.10    D1_1_1
#if defined LEITERPLATTE_263
//    #define SAMMELSTOERUNG          TRUE    //((PCS & 0x01) == 0)   // wird bei der IWS_HT nicht verwendet    
    #define SAMMELSTOER_OPTO              BSP_getDigitalInputState (BSP_DIGITAL_INPUT_1_2)                        //((PCS & 0x01) == 0)                    ST1.12     D1_1_2
    #define ABTAUEN_OPTO                  BSP_getDigitalInputState (BSP_DIGITAL_INPUT_1_3)                        //((PCS & 0x02) == 0)                    ST1.11     D1_1_3
#else
//    #define SAMMELSTOERUNG          TRUE   //((P9 & 0x4000) == 0)//P9.14 hat GND            
    #define ABTAUEN_OPTO                  BSP_getDigitalInputState (BSP_DIGITAL_INPUT_1_3)                          //((P9 & 0x8000) == 0)                ST1.12     D1_1_3
#endif    

// Hardwarezugriff f�r die SIL
#define ANZAHL_ANALOG_OUTPUTS           3   // 1 Ausgang f�r L�fter PWM, (1 Ausgang f�r EXV u. 1 Ausgang f�r EXV ZE siehe SIMULATION_VON_STE )
#define SIL_PWM_LUEFTER                 0   // cSOFTWARE_SIMULATION_30 gibt den Wert f�r die L�fterdrehzahl
#define SIL_EXV_OEFF                    1   // cSOFTWARE_SIMULATION_30 gibt den Wert f�r �ffnungsgrad des EXV Ventils
#define SIL_EXV_OEFF_ZE                 2   // cSOFTWARE_SIMULATION_32 gibt den Wert f�r �ffnungsgrad des EXV Ventils Zwischeneinspritzung vor
#define ANZAHL_DIGITAL_INPUTS           4   // 2 Opto �ber ADU (z�hlen hier nicht) 2 Opto �ber Port, 1 Volumenstrom Frequenz und ein Drehzahl PWM
#define SIL_DIGITAL_IN_ABTAU_OPTO       0
#define SIL_DIGITAL_IN_HD_OPTO          1
#define SIL_DIGITAL_IN_VOLSTR_OPTO      2
#define SIL_DIGITAL_IN_SAMMEL_OPTO      3
#define ANZAHL_ANALOG_INPUTS            2   // Volumenstrom (Huba-Sensor) und Drehzahleingang vom L�fter
#define SIL_ANALOG_IN_VOLUMENSTROM      0
#define SIL_ANALOG_IN_DREHZAHL          1


//======================== f�r externen AD-Wandler ADS7822 , 12-BIT-Aufl�sung ===========================================
    // Multiplexer              Inhibit-Bit sperrt MUX bei EIN
    #define FREIGABE_MUX_1             NO_OPERATION()                                //Nur 1 Multiplexer, INHIBIT auf MASSE ----> immer frei  BSP_waitOneNOP()
    #define FREIGABE_MUX_2             // nicht ben�tigt
    #define FREIGABE_MUX_3             // nicht ben�tigt
    
    #define SPERRE_MUX                 NO_OPERATION()                                //  _NOP();                                      //kein Abschalten erforderlich
    
    // Refer to BSP_ADC.h for other Multiplexer

    // externer AD-Wandler ADS7822

    #define DCLOCK_LOW          		      BSP_getDigitalOutputState (BSP_DIGITAL_OUTPUT_5_2, BSP_ACTOR_OFF)                //P9 &= ~0x0200//P9.9 = AUS;
    #define DCLOCK_HIGH         		      BSP_getDigitalOutputState (BSP_DIGITAL_OUTPUT_5_2, BSP_ACTOR_ON)                 //P9 |= 0x0200//P9.9 = EIN;
    
    #define ADC_CS_FREIGABE     		      BSP_getDigitalOutputState (BSP_DIGITAL_OUTPUT_5_3,  BSP_ACTOR_OFF)           //P9 &= ~0x0100//P9.8 = AUS;
    #define ADC_CS_SPERRE       		      BSP_getDigitalOutputState (BSP_DIGITAL_OUTPUT_5_3, BSP_ACTOR_ON)            //P9 |= 0x0100//P9.8 = EIN;
    
    #define ADC_READ            		      BSP_ADC_getExternalReadValue()                               //(P9 & 0x0080)//P9.7   //Dout am ADS7822
            
    #define AUFLOESUNG_ADW_EXTERN         8192//maximaler Z�hlwert bei 12-Bit-Wandler, bedingt durch Halbierung der Referenzspannung
    #define MAXWERT_ADW_EXTERN            4097//dieser Wert muss abgefragt werden, bedingt durch die doppelte Aufl�sung
                                          //sonst wird kein F�hlerbruch erkannt und die konfigs werden nicht gesetzt
    #define GRENZWERT_KURZSCHLUSS         4075
    #define GRENZWERT_BRUCH               20
    

//=========================== AD-Wandlung ===============================================================================

#if defined LEITERPLATTE_263
// f�r IWS_HT_STEP3 - neue Hardware ab Software Version 306_04_A --> auch neuer Prozessor V850-FX3L upd70F3619M2GBA 
    #define SER_WID1                      4020
    #define ANZAHL_AD_KANAELE             20  //12 am internen Wandler, 8 am ext. Wandler
    #define ANZAHL_MUX_KANAELE            8   // 8 Eing�nge am externen ADU
    #define ANZAHL_MUX                    1   //der INHIBIT liegt auf Masse


    #define MAX_AD_EINGANG                12          // nur f�r internen AD-Wandler
    #define ZWEI_AD_WANDLER

// Defines f�r den G�ltigkeitsbereich des Differenzdrucksensors
    #define OBERGRENZE_SPG_EINGANG        5.0     // STE kann die Grenzen nicht garantieren 4.6     // 4,6 Volt
    #define UNTERGRENZE_SPG_EINGANG       0.0     // STE kann die Grenzen nicht garantieren 0.4     // 0,4 Volt

    #if defined PT1000_VORLAUF_FUEHLER
        // externe AD-Wandlung
        #define     SAUGGASFUEHLER_VHD              0   // Pt1000 oder 5k-NTC, Auswertung �ber ext. ADU, MUX.X0 - ST2-Pin-1
        #define     VERDAMPFERFUEHLER               1   // Pt1000 oder 5k-NTC, Auswertung �ber ext. ADU, MUX.X1 - ST2-Pin-3
        #define     SAUGGASFUEHLER_VND              2   // Pt1000 oder 5k-NTC, Auswertung �ber ext. ADU, MUX.X2 - ST2-Pin-4
        #define     WP_VORLAUFFUEHLER               3   // Pt1000 oder 5k-NTC, Auswertung �ber ext. ADU, MUX.X3 - ST2-Pin-5
        #define     HEISSGASFUEHLER                 4   // Pt1000 oder 5k-NTC, Auswertung �ber ext. ADU, MUX.X4 - ST6-Pin-1
        #define     MD_SENSOR                       5   // 0...20mA,           Auswertung �ber ext. ADU, MUX.X5 - ST6-Pin-2
        #define     HD_SENSOR                       6   // 0...20mA,           Auswertung �ber ext. ADU, MUX.X6 - ST6-Pin-3
        #define     ND_SENSOR                       7   // 0...20mA,           Auswertung �ber ext. ADU, MUX.X7 - ST6-Pin-4
        
        // interne AD-Wandlung        
        #define     SENSOR_HD_STOERUNG              8   // Opto-Koppler,       Auswertung �ber int. ADU, P7.0 - ST1-Pin-9
        #define     IMPULSEINGANG_DURCHFLUSS        9   // Opto-Koppler,       Auswertung �ber int. ADU, P7.1 - ST1-Pin-10
        #define     WP_RUECKLAUFFUEHLER             10  // 2K-PTC-KTY81,       Auswertung �ber int. ADU, P7.2 - ST2-Pin-6
        #define     AUSSENFUEHLER                   11  // 2K-PTC-KTY81,       Auswertung �ber int. ADU, P7.3 - ST6-Pin-7
        #define     INVERTERSTROM_VHD               12  // 0...20mA,           Auswertung �ber int. ADU, P7.4 - ST9-Pin-4
        #define     INVERTERSTROM_VND               13  // 0...20mA,           Auswertung �ber int. ADU, P7.5 - ST9-Pin-5
        #define     FROSTSCHUTZFUEHLER              14  // 2K-PTC-KTY81,       Auswertung �ber int. ADU, P7.6 - ST10-Pin-1 
        #define     VERFLUESSIGERFUEHLER            15  // 2K-PTC-KTY81,       Auswertung �ber int. ADU, P7.7 - ST10-Pin-2 
        #define     FORTLUFTFUEHLER                 16  // 2K-PTC-KTY81,       Auswertung �ber int. ADU, P7.8 - ST10-Pin-3 
        #define     OELSUMPF_TEMP                   17  // 2K-PTC-KTY81		  Auswertung �ber int. ADU, P7.9 - ST10-Pin-4  
        #define     STROM_EINGANG                   18  // nicht spezifiziert  Auswertung �ber int. ADU, P7.10 - ST10-Pin-5 
        #define     SPG_EINGANG                     19  // Eingang 0...5V      Auswertung �ber int. ADU, P7.11 - ST10-Pin-6 
                    // STROM_EINGANG wird als Sensor f�r die Aufnahmeleistung der WP genutzt
                    // SPG_EINGANG wird als Differenzdrucksensor �ber den Verdampfer genutzt
                    
    #else   // Aussenf�hler als Pt1000

        // externe AD-Wandlung
        #define     SAUGGASFUEHLER_VHD              0   // Pt1000 oder 5k-NTC, Auswertung �ber ext. ADU, MUX.X0 - ST2-Pin-1
        #define     VERDAMPFERFUEHLER               1   // Pt1000 oder 5k-NTC, Auswertung �ber ext. ADU, MUX.X1 - ST2-Pin-3
        #define     SAUGGASFUEHLER_VND              2   // Pt1000 oder 5k-NTC, Auswertung �ber ext. ADU, MUX.X2 - ST2-Pin-4
        #define     AUSSENFUEHLER                   3   // Pt1000 oder 5k-NTC, Auswertung �ber ext. ADU, MUX.X3 - ST2-Pin-5
        #define     HEISSGASFUEHLER                 4   // Pt1000 oder 5k-NTC, Auswertung �ber ext. ADU, MUX.X4 - ST6-Pin-1
        #define     MD_SENSOR                       5   // 0...20mA,           Auswertung �ber ext. ADU, MUX.X5 - ST6-Pin-2
        #define     HD_SENSOR                       6   // 0...20mA,           Auswertung �ber ext. ADU, MUX.X6 - ST6-Pin-3
        #define     ND_SENSOR                       7   // 0...20mA,           Auswertung �ber ext. ADU, MUX.X7 - ST6-Pin-4
        
        // interne AD-Wandlung        
        #define     SENSOR_HD_STOERUNG              8   // Opto-Koppler,       Auswertung �ber int. ADU, P7.0 - ST1-Pin-9
        #define     IMPULSEINGANG_DURCHFLUSS        9   // Opto-Koppler,       Auswertung �ber int. ADU, P7.1 - ST1-Pin-10
        #define     WP_RUECKLAUFFUEHLER             10  // 2K-PTC-KTY81,       Auswertung �ber int. ADU, P7.2 - ST2-Pin-6
        #define     WP_VORLAUFFUEHLER               11  // 2K-PTC-KTY81,       Auswertung �ber int. ADU, P7.3 - ST6-Pin-7
        #define     INVERTERSTROM_VHD               12  // 0...20mA,           Auswertung �ber int. ADU, P7.4 - ST9-Pin-4
        #define     INVERTERSTROM_VND               13  // 0...20mA,           Auswertung �ber int. ADU, P7.5 - ST9-Pin-5
        #define     FROSTSCHUTZFUEHLER              14  // 2K-PTC-KTY81,       Auswertung �ber int. ADU, P7.6 - ST10-Pin-1 
        #define     VERFLUESSIGERFUEHLER            15  // 2K-PTC-KTY81,       Auswertung �ber int. ADU, P7.7 - ST10-Pin-2 
        #define     FORTLUFTFUEHLER                 16  // 2K-PTC-KTY81,       Auswertung �ber int. ADU, P7.8 - ST10-Pin-3 
        #define     OELSUMPF_TEMP                   17  // nicht spezifiziert  Auswertung �ber int. ADU, P7.9 - ST10-Pin-4  
        #define     STROM_EINGANG                   18  // nicht spezifiziert  Auswertung �ber int. ADU, P7.10 - ST10-Pin-5 
        #define     SPG_EINGANG                     19  // Eingang 0...5V      Auswertung �ber int. ADU, P7.11 - ST10-Pin-6 
                    // STROM_EINGANG wird als Sensor f�r die Aufnahmeleistung der WP genutzt
                    // SPG_EINGANG wird als Differenzdrucksensor �ber den Verdampfer genutzt
    #endif
#else

    #define SER_WID1                    4020
    #define ANZAHL_AD_KANAELE           12  //4 am internen Wandler, 8 am ext. Wandler
    #define ANZAHL_MUX_KANAELE          8   
    #define ANZAHL_MUX                  1   //der INHIBIT liegt auf Masse


    #define MAX_AD_EINGANG              4   // nur f�r internen AD-Wandler 
    #define ZWEI_AD_WANDLER

//========================== F�hleradressen =============================================================================
    // Definition der F�hleradressen bei internem und externem AD-Wandler 
    // Adressen 0 bis 1 f�r die Optokoppler HD und Impulseingang
    #define     SENSOR_HD_STOERUNG              8   // Eingang �ber Opto-Koppler, Auswertung �ber internen AD-Wandler P7.0
    #define     IMPULSEINGANG_DURCHFLUSS        9   // Eingang �ber Opto-Koppler, Auswertung �ber internen AD-Wandler P7.1 
    #define     WP_RUECKLAUFFUEHLER             10   // KTY81, Auswertung �ber internen AD-Wandler P7.2 - ST2-Pin-6
    #define     WP_VORLAUFFUEHLER               11   // KTY81, Auswertung �ber internen AD-Wandler P7.3 - ST6-Pin-7
        
    #define     SAUGGASFUEHLER_VHD              0   // Pt1000, Auswertung �ber externen AD-Wandler MUX.X0 - ST2-Pin-1
    #define     VERDAMPFERFUEHLER               1   // Pt1000, Auswertung �ber externen AD-Wandler MUX.X1 - ST2-Pin-3
    #define     SAUGGASFUEHLER_VND              2   // Pt1000, Auswertung �ber externen AD-Wandler MUX.X2 - ST2-Pin-4
    #define     AUSSENFUEHLER                   3   // Pt1000, Auswertung �ber externen AD-Wandler MUX.X3 - ST2-Pin-5   
    #define     HEISSGASFUEHLER                 4   // Pt1000, Auswertung �ber externen AD-Wandler MUX.X4 - ST6-Pin-1   
    #define     MD_SENSOR                       5   // 0...20mA, Auswertung �ber externen AD-Wandler MUX.X5 - ST6-Pin-2
    #define     HD_SENSOR                       6   // 0...20mA, Auswertung �ber externen AD-Wandler MUX.X6 - ST6-Pin-3
    #define     ND_SENSOR                       7   // 0...20mA, Auswertung �ber externen AD-Wandler MUX.X7 - ST6-Pin-4

#endif



// Watchdog triggern
#define mWATCHDOG()                   // BSP_WDG_Refresh()              // WDTE


#define mUPDATE_UHR()                    update_uhr()

#define mINTERRUPTFREIGABE()             BSP_interruptEnable()                  //_EI()

#define WAIT_1ys                         BSP_WAIT_1_MICROSECOND()
#define WAIT_2ys                         WAIT_1ys;\
                                         WAIT_1ys
#define WAIT_4ys                         WAIT_1ys;\
                                         WAIT_1ys;\
                                         WAIT_1ys;\
                                         WAIT_1ys
#define WAIT_5ys                         WAIT_1ys;\
                                         WAIT_1ys;\
                                         WAIT_1ys;\
                                         WAIT_1ys;\
                                         WAIT_1ys

#define WAIT_10ys	                       WAIT_5ys;\
                                         WAIT_5ys



#define WAIT_20ys	                       WAIT_10ys;\
                                         WAIT_10ys



#define USE_EEPROM_TWO_ADR_BYTE


#define IIC1_SOFTWARE_USED
#define EEPROM_IIC_INTERFACE	            IIC1_SOFTWARE_INTERFACE
//  Refer to I2C.h for other Makros

// 1 us ist die halbe High/Lowtime des I�C SCL Signals + Zeit zum Setzen bzw. Abfragen der Ports
// ausgemessene I�C Clock Frequenz:85 kHz
#define CC_IIC_WAITTIME_1                  BSP_I2C_waitOneMicroSecond();


// Wartezeit nach �betragen des letzten Bytes eines ModBus Frame
// > 1 Bitzeit: Bei 19200 Baud ist die Bitzeit 52 us
#define		WAIT_AFTER_TX			               WAIT_20ys;WAIT_20ys;WAIT_20ys;



