    // Anzeige der Versionsnummer, aendern in GERAETKENNUNG und VERSIONSNUMMER
#include "STE_CFG.h"   /* usage of  BSP_USE_V850   1  */
#if defined LEITERPLATTE_263

//		#define GERAETKENNUNG		 "IWS HT 306-07#"  //entspricht IWS HT 306-06
	#define GERAETKENNUNG		 "IWS 393-09#"  //entspricht IWS HT 306-06

	#define SOFTWARENUMMER          393
	#define VERSIONSNUMMER          9

	#define	   SOFTWARENUMMER_STE		  0x93  // erscheint im WPM2 / 6.9670.393    
	#define	   VERSIONSNUMMER_STE		  0x09  // Version ^
	
	// Entwicklungsstand
	#define DEVELOPMENT_ID		1	
	
    #define SOFTWARE_UNTERINDEX_1       ' '
    #define SOFTWARE_UNTERINDEX_2       'A'


#endif
    
    
    
#define	GENERALKUNDENKENNUNG	0
#define	KUNDENKENNUNG		    0	// Generalkundenkennung

#define	KASKADE
#if BSP_USE_V850  == 1
#if defined uPC70F3619
    #define CONTROLLER_HEADER       <io70F3619.h>       // V850 FX-3L
#elif defined uPC70F3373
    #define CONTROLLER_HEADER       <io70f3373.h>       // V850 FX-3
#else
    #define CONTROLLER_HEADER       <io70f3233.h>       // V850 FX-2
#endif
#endif

#if BSP_USE_V850  == 1
  #define INTRINSIC_FUNKTIONS     <intrinsics.h>  //<intrv850.h>
#endif
#define DATATYPES               <..\Definitionen\datatypes.h>
#define VAR_DEK                 <deklarationen.h>
#define APPL_DEF                <..\Definitionen\appl_def.h>
#define APPL_DISPLAY            <..\Display\appl_display.h>

// TO DO: 'Controller Komponente �ndern' 
//#define HEADER_PROZESSOR_MACRODRIVER CONTROLLER_HEADER
//#define HEADER_PROZESSOR_SERIAL CONTROLLER_HEADER				

#if defined LEITERPLATTE_263                                    // IWS_HT_STEP3
    #define EEPROM_PARAMETER_DEF    <eeprom_parameter.h> 
    #define EEPROM_DEF              <eeprom.h>
#else                                                           // IWS_HT_STEP2
    #define EEPROM_PARAMETER_DEF    <eeprom_parameter_step2.h> 
    #define EEPROM_DEF              <eeprom.h>
#endif

#define HARDWARE                <hardware.h>

#define PTC_HEADER              <..\Fuehler_PTC_NTC\fuehler_ptc.h>
#define NTC_HEADER              <..\Fuehler_PTC_NTC\fuehler_ntc.h>

#define FBR1_PTC_HEADER         <..\FBR1\fbr1_1010ptc.h>
#define E6_HEADER               <definitionen.h>
#define SPRACHEN                <sprachen.h>

#define HEADER_DATALOGGER       <datalogger.h>
#define HEADER_DATALOGGER_DEK   <datalogger_dek.h>


#define HEADER_IIC_SOFT			<..\Controller\iic_soft.h>	
#define HEADER_IIC_SOFT_EXTERN	<..\Controller\iic_soft_ext.h>
#define HEADER_SERIAL_ACCESS	<..\Controller\serial_access.h>

#define UHRENHEADER             <..\UHR\uhr.h>
#define HEADER_MISCHER          <..\Mischerregelung\mischer.h>
#define HEADER_IWS_MODCAN_DEF          <..\CAN\can_def.h>
#define HEADER_CAN_DEK          <..\CAN\can_dek.h>
#define HEADER_IWS_MOD_CAN              <..\CAN\can.h>


#define HEADER_EVE              <..\Geraet\local_eev\elektr_expansionsventil.h>
#define HEADER_EVE_DEK          <..\Geraet\local_eev\elektr_expansionsventil_ext_dek.h>
#define HEADER_EVE_DEF          <..\Geraet\local_eev\elektr_expansionsventil_def.h>
#define HEADER_RS232            <..\RS232\rs232.h>
#define HEADER_INFONUMMER		<..\Definitionen\infonummern.h>


#define HEADER_SIL_SIMULATION   <..\SIL_PC_SIMULATION\sil_simulation.h>
#define HEADER_STE_APPL_DEF     <..\Definitionen_OEM\Definitionen_Stiebel.h>	


#define HEADER_MB_DATA_LINK_COMMON		  <..\modbus\data_link_layer\data_link_layer_common.h>
#define HEADER_MB_DATA_LINK_EXT			  <..\modbus\data_link_layer\data_link_layer_common_ext.h>

#define HEADER_MB_APPL_DEF		          <..\modbus\application_layer\application_layer_appl_def.h>
#define HEADER_MB_APPL_LAYER	          <..\modbus\application_layer\application_layer_main.h>
#define HEADER_MB_APPL_DATA_DEF	          <..\modbus\application_layer\objekt_manager_data_def.h>

#define HEADER_SOA_DEF_EXT		  		  <..\Geraet\local_eev\verd_soa_ext.h>			

#define HEADER_DATA_POSTING_DEF			  <..\data_posting\posting_data_def.h>	
#define HEADER_DATA_POSTING_DEF_EXT		  <..\data_posting\posting_data_ext.h>	



// Definitionen fuer die Komunikation
#define cCAN_BUFFER_ANZAHL	          3
#define cMAX_CAN_SENDEBUFFERINDEX    120


// Wiederholungszeit von Kontrolltelegrammen Initialisierung u. Buskonf. 
#define	cCONTROL_REPEAT_TIME		  5 // Minuten 
#define TELEGRAMM_WIEDERHOLZEIT      10 // alle 10 Sekunden werden die Werte der Anzeigeebene aktualisiert 

// Definitionen fuer die Regelung
#define HEIZKREISNACHLAUF_MINUTEN     0
#define KESSELKREISNACHLAUF_MINUTEN   0

#define SPEICHERNACHLAUF_MINUTEN      5
#define SCHALTHYSTERESE_WW	       	 50

// Definitionen f�r die EEV-Regelung
//#define EXV_VOLLSCHRITTBETRIEB_1
#define EXV_VOLLSCHRITTBETRIEB_2
//#define EXV_HALBSCHRITTBETRIEB

//#define EXV_VOLLSCHRITTBETRIEB_ZE_1
#define EXV_VOLLSCHRITTBETRIEB_ZE_2
//#define EXV_HALBSCHRITTBETRIEB_ZE

// Umsetzung ATMEL->NEC
#define flash const


//AD-Wandlung extern mit 12-BIT-AD-Wandler, Referenzspannung 2500mV
//#define ADU_MIT_MUX_EXTERN      // externe AD-Wandlung

//AD-Wandler mit Referenzspannung 2500mV
#define HALBE_REFERENZSPANNUNG

//Einbindung der Datentypen fuer V850
#define V850_DATATYPES

//Mischerregelung
//#define EIN_MISCHERPARAMETER




//f�r die F�hler: Verdampferf�hler,Rekuperatorf�hler,Aussenf�hler,Einspritzf�hler als PT1000
#define PT1000_FUEHLER
//#define PT1000_VORLAUF_FUEHLER      // Wenn der Vorlauff�hler im Huba Sensor genutzt werden soll, ist dieser als Pt1000
                                    // auszuwerten. Da aber der Eingang daf�r nicht ausgelegt ist, wird der Kanal mit dem
                                    // Aussenf�hler getauscht.
                                    
// f�r das PTC-NTC F�hlermodul      // Auswertung 2k PTC immer und zus�tzlich Auswertung 5k NTC oder Pt1000
#define FUEHLER_PTC_AUSWERTUNG
#define FUEHLER_PTC_2K_KTY81
#define FUEHLER_KTY_81_INFLECTION
#define FUEHLER_PTC_PT1000
#ifndef PT1000_FUEHLER
    #define FUEHLER_NTC_AUSWERTUNG
    #define FUEHLER_NTC_5K
#endif



//IWS mit PWM-Ausgang
#define IWS_PWM

#define VERSION_EEV
#define DATALOGGER_STE
#define ANLAUFHILFE_VERDICHTER_ND

// Definition f�r das bedingte Compilieren der PC-Simulation
//#define SIL_PC_SIMULATION         // geht hier leider nicht anders, da hier die AD-Wert noch geschoben werden
#define SIL_PC_SIM_SONDER
#define SIL_ANALOG_OUTPUT
#define SIL_ANALOG_INPUT
#define SIL_DIGITAL_INPUT

// M�glichkeit f�r STE auf Stell- und Regelgr��en zuzugreifen ( --> Kundenwunsch von M. Herrs )                                     
#define SIMULATION_VON_STE
#define STE_SIM_TIMEOUTZEIT 30  // 30s


//#define TEST_AUSGANGSTAKT // beim V850

//#include DATATYPES
#include "datatypes.h"
//#include E6_HEADER
//#include APPL_DEF
#include "appl_def.h"
//#include HEADER_STE_APPL_DEF
//#include HEADER_INFONUMMER
//#include HARDWARE
#include "hardware.h"
//#include VAR_DEK
//#if BSP_USE_V850  == 1
//   #include INTRINSIC_FUNKTIONS
//   #include CONTROLLER_HEADER
//#endif
//#include HEADER_SERIAL_ACCESS
//#include HEADER_MB_APPL_DEF
