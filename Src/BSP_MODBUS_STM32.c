

/* Includes =============================================================== */
#include "STE_CFG.h"
#if BSP_USE_STM32 == 1
#include "BSP_MODBUS.h"

/* Private defines ======================================================== */

/* Private macros ========================================================= */

/* Private typedefs ======================================================= */

/* Private variables ====================================================== */

/* Public variables ======================================================= */

/* Private function prototypes ============================================ */

/* Private functions ====================================================== */

/* Public functions ======================================================= */

 /**
  ******************************************************************************
  * @brief   Initialize the UART for Modbus communication
  * @note    Function is called from the Data Link Layer
  * @param   Baud rate and Parity
  * @return  MOD_BUS_OK
  ******************************************************************************
 */
int8_t    BSP_MODBUS_initUart(BSP_MODBUS_e_BaudRate_t e_Baudrate, BSP_MODBUS_e_Parity_t e_Parity)
{
	/* Enable the peripheral clock of GPIO Port */
	USARTx_GPIO_CLK_ENABLE();

	/* Configure Tx Pin as : Alternate function, High Speed, Push pull, Pull up */
	LL_GPIO_SetPinMode(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_MODE_ALTERNATE);
	USARTx_SET_TX_GPIO_AF();
	LL_GPIO_SetPinSpeed(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_SPEED_FREQ_HIGH);
	LL_GPIO_SetPinOutputType(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_OUTPUT_PUSHPULL);
	LL_GPIO_SetPinPull(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_PULL_UP);

	/* Configure Rx Pin as : Alternate function, High Speed, Push pull, Pull up */
	LL_GPIO_SetPinMode(USARTx_RX_GPIO_PORT, USARTx_RX_PIN, LL_GPIO_MODE_ALTERNATE);
	USARTx_SET_RX_GPIO_AF();
	LL_GPIO_SetPinSpeed(USARTx_RX_GPIO_PORT, USARTx_RX_PIN, LL_GPIO_SPEED_FREQ_HIGH);
	LL_GPIO_SetPinOutputType(USARTx_RX_GPIO_PORT, USARTx_RX_PIN, LL_GPIO_OUTPUT_PUSHPULL);
	LL_GPIO_SetPinPull(USARTx_RX_GPIO_PORT, USARTx_RX_PIN, LL_GPIO_PULL_UP);

	/* (2) NVIC Configuration for USART interrupts */
	/*  - Set priority for USARTx_IRQn */
	/*  - Enable USARTx_IRQn */
	NVIC_SetPriority(USARTx_IRQn, 0);
	NVIC_EnableIRQ(USARTx_IRQn);

	/* (3) Enable USART peripheral clock and clock source ***********************/
	USARTx_CLK_ENABLE();

	/* Set clock source */
	USARTx_CLK_SOURCE();

	/* (4) Configure USART functional parameters ********************************/

	/* Disable USART prior modifying configuration registers */
	/* Note: Commented as corresponding to Reset value */
	// LL_USART_Disable(USARTx_INSTANCE);

	/* TX/RX direction */
	LL_USART_SetTransferDirection(USARTx_INSTANCE, LL_USART_DIRECTION_TX_RX);

	/* 8 data bit, 1 start bit, 1 stop bit and select parity */
	switch(e_Parity)
	{
	case MOD_BUS_NO_PARITY:
		// UD0CTL0 = UARTD_TRANSFDIR_LSB | UARTD_PARITY_NONE | UARTD_DATALENGTH_8BIT | UARTD_STOPLENGTH_1BIT;
		LL_USART_ConfigCharacter(USARTx_INSTANCE, LL_USART_DATAWIDTH_8B, LL_USART_PARITY_NONE, LL_USART_STOPBITS_1);
		break;

	case MOD_BUS_ODD_PARITY:
		// UD0CTL0 = UARTD_TRANSFDIR_LSB | UARTD_PARITY_ODD | UARTD_DATALENGTH_8BIT | UARTD_STOPLENGTH_1BIT;
		LL_USART_ConfigCharacter(USARTx_INSTANCE, LL_USART_DATAWIDTH_8B, LL_USART_PARITY_ODD, LL_USART_STOPBITS_1);
		break;

	case MOD_BUS_EVEN_PARITY:
		// UD0CTL0 = UARTD_TRANSFDIR_LSB | UARTD_PARITY_EVEN | UARTD_DATALENGTH_8BIT | UARTD_STOPLENGTH_1BIT;
		LL_USART_ConfigCharacter(USARTx_INSTANCE, LL_USART_DATAWIDTH_8B, LL_USART_PARITY_EVEN, LL_USART_STOPBITS_1);
		break;
	} // endswitch

	/* No Hardware Flow control */
	/* Reset value is LL_USART_HWCONTROL_NONE */
	// LL_USART_SetHWFlowCtrl(USARTx_INSTANCE, LL_USART_HWCONTROL_NONE);

	/* Oversampling by 16 */
	/* Reset value is LL_USART_OVERSAMPLING_16 */
	// LL_USART_SetOverSampling(USARTx_INSTANCE, LL_USART_OVERSAMPLING_16);

	/* Set Baudrate to 115200 using APB frequency set to 80000000 Hz */
	/* Frequency available for USART peripheral can also be calculated through LL RCC macro */
	/* Ex : Periphclk = LL_RCC_GetUSARTClockFreq(Instance); or LL_RCC_GetUARTClockFreq(Instance); depending on USART/UART instance */
	/* In this example, Peripheral Clock is expected to be equal to 80000000 Hz => equal to SystemCoreClock */
	switch(e_Baudrate)
	{
	case MOD_BUS_DEFAULT_BAUD:
	case MOD_BUS_BAUD_19200:
		LL_USART_SetBaudRate(USARTx_INSTANCE, SystemCoreClock, LL_USART_OVERSAMPLING_16, 19200);
		break;

	case MOD_BUS_BAUD_9600:
		LL_USART_SetBaudRate(USARTx_INSTANCE, SystemCoreClock, LL_USART_OVERSAMPLING_16, 9600);
		break;

	default:
		return(MOD_BUS_INIT_FAIL);
	}

	/* Enable USART */
	LL_USART_Enable(USARTx_INSTANCE);

	/* Polling USART initialisation */
	while((!(LL_USART_IsActiveFlag_TEACK(USARTx_INSTANCE))) || (!(LL_USART_IsActiveFlag_REACK(USARTx_INSTANCE))))
	{
	}

#if 0
	u16_BusDebugState |= MOD_BUS_DBG_DATA_LINK_INIT_UART_ENTRY;

	UD0TXE = 0; /* UART0: Transmit ausschalten */
	UD0RXE = 0; /* UART0: Receive ausschalten */
	UD0PWR = 0; /* UART0: Funktion ausschalten */
	UD0TMK = 1; /* UART0: Tx Interrupt ausschalten */
	UD0TIF = 0; /* UART0: TX Interrupt Flag zur�cksetzen */
	UD0RMK = 1; /* UART0: Rx Interrupt ausschalten */
	UD0RIF = 0; /* UART0: RX Interrupt Flag zur�cksetzen */
	UD0SMK = 1; /* UART0: Status Interrupt ausschalten */
	UD0SIF = 0; /* UART0: Status Interrupt Flag zur�cksetzen */

	/* UART0: niedrigste Prio f�r TX Interrupt */
	UD0TIC |= 0x07;
	/* UART0: hoechste Prio f�r RX Interrupt */
	UD0RIC &= ~0x07;
	/* UART0: niedrigste Prio f�r Status Interrupt */
	UD0SIC |= 0x07;

	/** Auswahl der Baudrate */
	switch(e_Baudrate)
	{
	case MOD_BUS_DEFAULT_BAUD:
	case MOD_BUS_BAUD_19200:
		/* fxp1 = 4 MHZ, fuclk = 4 MHZ / 8 = 500 kHz, baudrate = fuclk / (2*CTL2) = 500 khz / 26 = 19231 baud => 0.16 % Fehler */
		UD0CTL1 = UARTD_BASECLK_FXP1_8;
		UD0CTL2 = UARTD0_BASECLK_DIVISION;
		ISEL26 = 0; /* selection of UARTD0 counter clock = fXP1 */
		break;

	case MOD_BUS_BAUD_9600:
		/* fxp1 = 4 MHZ, fuclk = 4 MHZ / 16 = 250 kHz, baudrate = fuclk / (2*CTL2) = 250 khz / 26 = 9615 baud => 0.16 % Fehler */
		UD0CTL1 = UARTD_BASECLK_FXP1_16;
		UD0CTL2 = UARTD0_BASECLK_DIVISION;
		ISEL26 = 0; /* selection of UARTD0 counter clock = fXP1 */
		break;

	default:
		return(MOD_BUS_INIT_FAIL);
	} // endswitch


	/** Auswahl der Parity */
	switch(e_Parity)
	{
	case MOD_BUS_NO_PARITY:
		UD0CTL0 = UARTD_TRANSFDIR_LSB | UARTD_PARITY_NONE | UARTD_DATALENGTH_8BIT | UARTD_STOPLENGTH_1BIT;
		break;

	case MOD_BUS_ODD_PARITY:
		UD0CTL0 = UARTD_TRANSFDIR_LSB | UARTD_PARITY_ODD | UARTD_DATALENGTH_8BIT | UARTD_STOPLENGTH_1BIT;
		break;

	case MOD_BUS_EVEN_PARITY:
		UD0CTL0 = UARTD_TRANSFDIR_LSB | UARTD_PARITY_EVEN | UARTD_DATALENGTH_8BIT | UARTD_STOPLENGTH_1BIT;
		break;
	} // endswitch

	/* UARTD0 TXDD0 pin set */
	PMC3L |= 0x01;
	/* UARTD0 RXDD0 pin set */
	PMC3L |= 0x02;

	/* Interval Timer f�r �berwachung Interbyte Timeout (t15) bzw. Frame Pause (t35) initialisieren, aber noch nicht starten */
	TAA0CE = 0;   /* TAA0 operation disable */
	TAA0CCMK0 = 1;  /* INTTAA0CC0 interrupt disable */
	TAA0CCIF0 = 0;  /* clear INTTAA0CC0 interrupt flag */
	TAA0OVMK = 1; /* INTTAA0OV interrupt disable */
	TAA0OVIF = 0; /* clear INTTAA0OV interrupt flag */

	ISEL20 = 0; /* selection of TAA0 counter clock = fXP1 */
	/* Interval timer mode setting */
	TAA0CTL0 = 0x00;          /* fxp1: 4MHZ */
	TAA0CTL1 = 0x00;          /* Intervall Timer */
	TAA0CCR0 = 0xffff;          /* Vergleichswert wird durch ModBus Status Maschine gesetzt */


	/* RS 485 Transceiver Chip LTC 1535CSW: TX und Rx enable */
	/* PCM2 und PCM3 als Ausgang */
	PMCM = 0xFF & (~0x0C);
	/* PCM im Port Mode */
	PMCCM = 0x00;
	/* PCM2 low, PCM3 high */
	MODBUS_TX_OFF_RX_OFF


	/*  UART0 starten
     RX Interrupt und Status Interrupt aktivieren
     TX Interrupt bleibt aus, es wird auf das Interuptflag gepollt
	 */
	UD0TIF = 0; /* clear INTUD0T interrupt flag */
	UD0RIF = 0; /* clear INTUD0R interrupt flag */
	UD0RMK = 0; /* INTUD0R interrupt enable */
	UD0SIF = 0; /* clear INTUD0S interrupt flag */
	//  UD0SMK = 0; /* INTUD0S interrupt enable */

	UD0PWR = 1; /* enable UARTD0 operation */
	UD0TXE = 1; /* enable transmission operation(uartd0) */
	UD0RXE = 1; /* enable reception operation(uartd0) */

	MODBUS_SET_TIMER_VALUE(u16_FramePauseTimerValue)

	u16_BusDebugState |= MOD_BUS_DBG_DATA_LINK_INIT_UART_EXIT;

#endif
	return(MOD_BUS_OK);
}

void      BSP_MODBUS_init(void)
{
	;
}

void      BSP_Modbus_stateMachine(void)
{
	;
}

uint16    BSP_MODBUS_getDebugState(void)
{
	return 0;
}

void      BSP_MODBUS_setDebugState(uint16 state_u16)
{
	;
}

void      BSP_MODBUS_setInterbyteTimeout(uint16_t   u16_NewInterbyteTimeout)
{
	;
}

void      BSP_MODBUS_100msAktionen(void)
{
	;
}

uint16    BSP_MODBUS_get100msCounter(void)
{
	return 0;
}

void      BSP_MODBUS_setDataPtr(BSP_MODBUS_st_ModBusData_t* pst_NewModBusData)
{
	;
}

void      BSP_MODBUS_setFramePause(uint16 u16_NewFramePauseTimerValue)
{
	;
}
#endif

/* End of BSP_MODBUS_STM32.c ==================================================== */
