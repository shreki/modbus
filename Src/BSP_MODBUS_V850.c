

/* Includes =============================================================== */
#include "STE_CFG.h"
#if BSP_USE_V850 == 1
#include "BSP_MODBUS.h"
#include "BSP_WDG.h"
#include "Hardware.h"

/* Private defines ======================================================== */
#define   MODBUS_TIME_PER_CNT         0.25      /**<  Zeit in us pro Count des f�r ModBus Timeout �berwachung benutzten Intervall Timers */
                                                /**< hier bei 4 MHz: 0.25 us */
#define   MODBUS_TIMER_START          TAA0CE = 1;
#define   MODBUS_TIMER_STOP           TAA0CE = 0;TAA0CCIF0 = 0;
#define   MODBUS_TIMER_IRQ_FLAG       TAA0CCIF0
#define   MODBUS_SET_TIMER_VALUE(a)   TAA0CE = 0;TAA0CCR0=a;TAA0CCIF0 = 0;TAA0CE = 1;
#define   MODBUS_TX_OFF_RX_OFF        PCM &= ~0x04;PCM |= 0x08;
//  #define   MODBUS_TX_OFF_RX_ON         PCM &= ~0x0C;
#define   MODBUS_TX_OFF_RX_ON         PCM &= ~0x04;WAIT_1ys;PCM &= ~0x08;
//  #define   MODBUS_TX_ON_RX_OFF         PCM |= 0x0C;
#define   MODBUS_TX_ON_RX_OFF         PCM |= 0x08;WAIT_1ys;PCM |= 0x04;     /**< RX OFF, 1 us warten, TX ON  */
#define   UART_PARITY_ERR             0x04
#define   UART_FRAMING_ERR            0x02
#define   UART_OVERRUN_ERR            0x01
#define   MOD_BUS_INTERBYTE_TIMEOUT   0x08

/*
	UARTDn control register 0(UDnCTL0)
*/

#define	UARTD_UDnCTL0_INITIALVALUE		0x10

/**< UARTDn operation control(UDnPWR) */
#define	UARTD_OPERATION_DISABLE			  0x00          /**< Disable UARTDn operation (UARTDn reset asynchronously) */
#define	UARTD_OPERATION_ENABLE			  0x80          /**< Enable UARTDn operation */

/**< Transmission operation enable(UDnTXE) */
#define	UARTD_TRANSMISSION_DISABLE		0x00          /**< Disable transmission operation */
#define	UARTD_TRANSMISSION_ENABLE		  0x40          /**< Enable transmission operation */

/**< Reception operation enable(UDnRXE) */
#define	UARTD_RECEPTION_DISABLE			  0x00          /**< Disable reception operation */
#define	UARTD_RECEPTION_ENABLE			  0x20          /**< Enable reception operation */

/**< Transfer direction selection(UDnDIR) */
#define	UARTD_TRANSFDIR_MSB				    0x00            /**< MSB-first transfer */
#define	UARTD_TRANSFDIR_LSB				    0x10            /**< LSB-first transfer */

/**< Parity selection during transmission/reception(UDnPS1,UDnPS0) */
#define	UARTD_PARITY_NONE				       0x00           /**< No parity output/Reception with no parity */
#define	UARTD_PARITY_ZREO				       0x04           /**< 0 parity output/Reception with 0 parity */
#define	UARTD_PARITY_ODD				       0x08           /**< Odd parity output/Odd parity check */
#define	UARTD_PARITY_EVEN				       0x0C          	/**< Even parity output/Even parity check */

/**< Specification of data character length of 1 frame of transmit/receive data(UDnCL) */
#define	UARTD_DATALENGTH_7BIT			     0x00          	/**< 7 bits */
#define	UARTD_DATALENGTH_8BIT			     0x02          	/**< 8 bits */

/**< Specification of length of stop bit for transmit data(UDnSL) */
#define	UARTD_STOPLENGTH_1BIT			     0x00	          /**< 1 bit */
#define	UARTD_STOPLENGTH_2BIT			     0x01	          /**< 2 bits */

/*
	UARTDn base clock selects register(UDnCTL1)
*/

/* UDnCTL1 register(UDnCKS3, UDnCKS2, UDnCKS1, UDnCKS0) */
#define	UARTD_BASECLK_FXP1_1			  0x00	  /**< fXP1 */
#define	UARTD_BASECLK_FXP2_1			  0x00	  /**< fXP2 */
#define	UARTD_BASECLK_FXP1_2			  0x01	  /**< fXP1/2 */
#define	UARTD_BASECLK_FXP1_4			  0x02	  /**< fXP1/4 */
#define	UARTD_BASECLK_FXP1_8			  0x03	  /**< fXP1/8 */
#define	UARTD_BASECLK_FXP1_16			  0x04	  /**< fXP1/16 */
#define	UARTD_BASECLK_FXP1_32		 	  0x05	  /**< fXP1/32 */
#define	UARTD_BASECLK_FXP1_64			  0x06	  /**< fXP1/64 */
#define	UARTD_BASECLK_FXP1_128			0x07	  /**< fXP1/128 */
#define	UARTD_BASECLK_FXP1_256			0x08	  /**< fXP1/256 */
#define	UARTD_BASECLK_FXP1_512			0x09	  /**< fXP1/512 */
#define	UARTD_BASECLK_FXP1_1024			0x0A	  /**< fXP1/1024 */

/* Selection of 8-bit counter output clock (UD0BRS7~UD0BRS0) */
#define	UARTD0_BASECLK_DIVISION		  0xd	    /**< 4 ~ 255 */


/* Private macros ========================================================= */


/* Private typedefs ======================================================= */

/* Private variables ====================================================== */
static uint16_t    u16_FramePauseTimerValue = 0;
static BSP_MODBUS_st_ModBusData_t * pst_ModBusData = 0;            /**< Zeiger auf ModBus Datenstruktue in 'data_link_layer.c */
static uint8_t     au8_ReadData[NUM_MOD_BUS_DATA_BYTES + 4] = {0};   /**< +4 wg. Slave Adr, FuncCode und 2 x CRC Bytes */
static uint8_t     u8_NumExecBytes = 0;                        /**< Anzahl verarbeiter Bytes in au8_ReadData */
static uint8_t     u8_NumReadBytes = 0;                        /**< Anzahl gelesener Bytes in au8_ReadData */
static uint8_t     u8_NumRegBytes = 0;                         /**< Anzahl erwarteter Antwort Bytes */
static uint8_t     u8_BytesToEval = 0;                         /**< Anzahl noch nicht verarbeiter Bytes in 'au8_ReadData' ab Index 'u8_NumReadBytes' */
static uint8_t     u8_UartErrorType = 0;                       /**< Durch Status Interrupt gemeldeter UART Fehler */
static uint8_t     u8_FramePauseOk = FALSE;
static uint16_t    u16_100msCounter;
static uint16_t    u16_100msFrameOkValue = 25;
static uint16_t    u16_100msFrameOkCnt = 0;
static uint16_t    u16_BusDebugState = 0;
static uint16_t	   u16_InterbyteTimeoutValue = 0;		           /**< Vergleichswert des Intervall Timers f�r t15 */
static uint16_t    u16_BusDebugState;

/* Public variables ======================================================= */

/* Private function prototypes ============================================ */

/* Private functions ====================================================== */
/**
 ******************************************************************************
 * @brief   Send the Modbus Data through the  UART
 * @note    Function is called from the Data Link Layer state machine
 * @param   Communication data structure
 * @return  MOD_BUS_OK
 ******************************************************************************
*/
static int8_t SendData(BSP_MODBUS_st_ModBusData_t * pst_ModBusData)
{
  uint8_t bytes_to_send_u8[NUM_MOD_BUS_DATA_BYTES + 4] = {0};  // +4 wg. Slave Adr, FuncCode und 2 x CRC Bytes
    uint16_t byte_cnt_u8 = 0;
    uint16_t num_echo_bytes_u16 = 0;
    uint16_t i;
    uint16_t crc16_val_u16;

    // passt die Anzahl zu schreibener Bytes ?
    if( pst_ModBusData -> num_write_data_bytes_u16 > NUM_MOD_BUS_DATA_BYTES + 4)
    {
      return(MOD_BUS_UART_WRITE_ERR);
    } // endif

    // Telegramm zusammenbauen
    // Slave Adresse und Funktionscode
    bytes_to_send_u8[0] = pst_ModBusData -> slave_adr_u8;
    bytes_to_send_u8[1] = pst_ModBusData -> func_code_u8;
    byte_cnt_u8 += 2;

    // Daten Bytes
    for(i = 0; i < pst_ModBusData -> num_write_data_bytes_u16; i++)
    {
      bytes_to_send_u8[2 + i] = (pst_ModBusData -> data_u8)[i];
    } // endfor
    byte_cnt_u8 += pst_ModBusData -> num_write_data_bytes_u16;

    // CRC Wert errechnen
    crc16_val_u16 = calc_CRC16(pst_ModBusData, pst_ModBusData -> num_write_data_bytes_u16);

    // Checksumme anh�ngen (LSB first)
    bytes_to_send_u8[byte_cnt_u8] = (uint8_t) (crc16_val_u16 & 0xFF);
    bytes_to_send_u8[byte_cnt_u8 + 1] = (uint8_t) ((crc16_val_u16 >> 8) & 0xFF);
    byte_cnt_u8 += 2;


    // LTC1535: DE Pin auf High. Senden m�glich, Empfang sperren
    UD0RMK = 1; /* INTUD0R interrupt disable */
    MODBUS_TX_ON_RX_OFF

    #ifdef CAN_INTERRUPT_GESPERRT
      CAN_INTERRUPT_GESPERRT;
    #endif

    // Daten versenden
    #if defined uPC70F3619
    #if defined  MOD_BUS_UART0


      // warten bis UART frei
      UD0TIF = 0;
      while(UD0TIF == 1);
          while(UD0TSF == 1);

      // UART Status Register zur�cksetzen
          UD0STR &= (uint8_t)~0x07;
          UD0SIF = 0;
          au8_ReadData[0] = UD0RX;


      mWATCHDOG();
      for(i = 0; i < byte_cnt_u8; i++)
      {
        // neues Byte zuweisen
        UD0TX = bytes_to_send_u8[i];
        // warten bis Byte versendet
        while(UD0TIF == 0);
        UD0TIF = 0;
        mWATCHDOG();
      } // endfor

      // warten bis letztes Byte versendet
          while(UD0TSF == 1);

      // Wartezeit nach �betragen des letzten Bytes eines ModBus Frame
      // UD0TSF wird 0 bevor das Stopbit komplett �bertragen ist
      // > 1 Bitzeit: Bei 19200 Baud ist die Bitzeit 52 us -> Zeit auf 60 us
      WAIT_AFTER_TX
    #endif
    #endif

    // LTC1535: DE Pin auf low. Empfang m�glich
    MODBUS_TX_OFF_RX_ON

    // 5 us warten, bis RX Interrupt scharf; Zeit muss kleiner sein als Mod Bus Frame Pause
    WAIT_5ys;
    UD0RIF = 0;           // clear INTUD0R interrupt flag
    au8_ReadData[0] = UD0RX;     // Dummy Read um RX Register zu leeren
    UD0STR &= (uint8_t)~0x07;        // UART Status Register zur�cksetzen
    UD0SIF = 0;
    UD0RMK = 0; /* INTUD0R interrupt ensable */


    #ifdef CAN_INTERRUPT_GESPERRT
      CAN_INTERRUPT_FREI;
    #endif

    return(MOD_BUS_OK);
}

/* Public functions ======================================================= */

/**
 ******************************************************************************
 * @brief Reset the data pointers
 * @note Must be call after init to avoid dangling pointers
 *   @param  none
 *   @return none
 ******************************************************************************
*/
void BSP_MODBUS_init(void)
{
   /**<  Only V850 speficific initialisation */

}

/**
 ******************************************************************************
 * @brief Set u16_InterbyteTimeoutValue
 * @note Must be before using of the pointer
 * @param  none
 * @return none
 ******************************************************************************
*/
void BSP_MODBUS_setInterbyteTimeout(uint16_t u16_newInterbyteTimeout)
{
	// Intervall Timer l�uft mit 4 MHZ -> 0.25 us pro Count
		u16_InterbyteTimeoutValue = (uint16_t) ((FLP32) u16_newInterbyteTimeout / (FLP32) MODBUS_TIME_PER_CNT);
}

/**
 ******************************************************************************
 * @brief   Read a variable from module posting_group_5_data_def
 * @note none
 * @param  none
 * @return u16_100msCounter
 ******************************************************************************
*/
uint16    BSP_MODBUS_get100msCounter( void )
{
  return(u16_100msCounter);
}


/**
 ******************************************************************************
 * @brief   Set a new frame delay
 * @note none
 * @param  the new delay
 * @return none
 ******************************************************************************
*/
void BSP_MODBUS_setFramePause(uint16 u16_NewFramePauseTimerValue)
{
    // Intervall Timer l�uft mit 4 MHZ -> 0.25 us pro Count
  u16_FramePauseTimerValue = (uint16) ((float32_t) u16_NewFramePauseTimerValue / (float32_t) MODBUS_TIME_PER_CNT);
}

/**
 ******************************************************************************
 * @brief   Read the debug state
 * @note none
 * @param  none
 * @return u16_BusDebugState
 ******************************************************************************
*/
uint16 BSP_MODBUS_getDebugState( void )
{
  uint16 u16_RetValue;

  u16_RetValue = u16_BusDebugState;
  u16_BusDebugState = 0;

  return(u16_RetValue);
}


/**
 ******************************************************************************
 * @brief   Set the debug state
 * @note none
 * @param  new state
 * @return none
 ******************************************************************************
*/
void BSP_MODBUS_setDebugState( uint16 u16_State)
{
  u16_BusDebugState |= u16_State;
}


/**
 ******************************************************************************
 * @brief   Change the 100ms Counters
 * @note   none
 * @param  none
 * @return none
 ******************************************************************************
*/
void BSP_MODBUS_100msAktionen( void )
{
  if(pst_ModBusData != 0)
  {
    if(u16_100msCounter > 0 && pst_ModBusData -> state_enm == MOD_BUS_COMM_IN_PROGRESS)
    {
      u16_100msCounter--;
    } // endif

    if(u16_100msFrameOkCnt < 0xFFFF)
    {
      u16_100msFrameOkCnt++;
    }
  }
}

/**
 ******************************************************************************
 * @brief   Set the pointer of the of Modbus
 * @note    Export of data
 * @param   The data pointer used by the caller
 * @return  none
 ******************************************************************************
*/
void BSP_MODBUS_setDataPtr(BSP_MODBUS_st_ModBusData_t* pst_NewModBusData)
{
  pst_ModBusData = pst_NewModBusData;
}


/**
 ******************************************************************************
 * @brief Initialize the UART for Modbus communication
 * @note    Function is called from the Data Link Layer
 * @param   Baud rate and Parity
 * @return  MOD_BUS_OK
 ******************************************************************************
*/
int8_t BSP_MODBUS_initUart(BSP_MODBUS_e_BaudRate_t    e_Baudrate,  BSP_MODBUS_e_Parity_t   e_Parity)
{
#if MOD_BUS_UART0 == 1

  u16_BusDebugState |= MOD_BUS_DBG_DATA_LINK_INIT_UART_ENTRY;

   UD0TXE = 0; /* UART0: Transmit ausschalten */
   UD0RXE = 0; /* UART0: Receive ausschalten */
   UD0PWR = 0; /* UART0: Funktion ausschalten */
   UD0TMK = 1; /* UART0: Tx Interrupt ausschalten */
   UD0TIF = 0; /* UART0: TX Interrupt Flag zur�cksetzen */
   UD0RMK = 1; /* UART0: Rx Interrupt ausschalten */
   UD0RIF = 0; /* UART0: RX Interrupt Flag zur�cksetzen */
   UD0SMK = 1; /* UART0: Status Interrupt ausschalten */
   UD0SIF = 0; /* UART0: Status Interrupt Flag zur�cksetzen */

   /* UART0: niedrigste Prio f�r TX Interrupt */
   UD0TIC |= 0x07;
   /* UART0: hoechste Prio f�r RX Interrupt */
   UD0RIC &= ~0x07;
   /* UART0: niedrigste Prio f�r Status Interrupt */
   UD0SIC |= 0x07;

   /** Auswahl der Baudrate */
   switch(e_Baudrate)
   {
     case MOD_BUS_DEFAULT_BAUD:
     case MOD_BUS_BAUD_19200:
       /* fxp1 = 4 MHZ, fuclk = 4 MHZ / 8 = 500 kHz, baudrate = fuclk / (2*CTL2) = 500 khz / 26 = 19231 baud => 0.16 % Fehler */
       UD0CTL1 = UARTD_BASECLK_FXP1_8;
       UD0CTL2 = UARTD0_BASECLK_DIVISION;
       ISEL26 = 0; /* selection of UARTD0 counter clock = fXP1 */
       break;

     case MOD_BUS_BAUD_9600:
       /* fxp1 = 4 MHZ, fuclk = 4 MHZ / 16 = 250 kHz, baudrate = fuclk / (2*CTL2) = 250 khz / 26 = 9615 baud => 0.16 % Fehler */
       UD0CTL1 = UARTD_BASECLK_FXP1_16;
       UD0CTL2 = UARTD0_BASECLK_DIVISION;
       ISEL26 = 0; /* selection of UARTD0 counter clock = fXP1 */
       break;

     default:
         return(MOD_BUS_INIT_FAIL);
   } // endswitch


   /** Auswahl der Parity */
   switch(e_Parity)
   {
     case MOD_BUS_NO_PARITY:
       UD0CTL0 = UARTD_TRANSFDIR_LSB | UARTD_PARITY_NONE | UARTD_DATALENGTH_8BIT | UARTD_STOPLENGTH_1BIT;
       break;

     case MOD_BUS_ODD_PARITY:
       UD0CTL0 = UARTD_TRANSFDIR_LSB | UARTD_PARITY_ODD | UARTD_DATALENGTH_8BIT | UARTD_STOPLENGTH_1BIT;
       break;

     case MOD_BUS_EVEN_PARITY:
       UD0CTL0 = UARTD_TRANSFDIR_LSB | UARTD_PARITY_EVEN | UARTD_DATALENGTH_8BIT | UARTD_STOPLENGTH_1BIT;
       break;
   } // endswitch

   /* UARTD0 TXDD0 pin set */
   PMC3L |= 0x01;
   /* UARTD0 RXDD0 pin set */
   PMC3L |= 0x02;

   /* Interval Timer f�r �berwachung Interbyte Timeout (t15) bzw. Frame Pause (t35) initialisieren, aber noch nicht starten */
   TAA0CE = 0;   /* TAA0 operation disable */
   TAA0CCMK0 = 1;  /* INTTAA0CC0 interrupt disable */
   TAA0CCIF0 = 0;  /* clear INTTAA0CC0 interrupt flag */
   TAA0OVMK = 1; /* INTTAA0OV interrupt disable */
   TAA0OVIF = 0; /* clear INTTAA0OV interrupt flag */

   ISEL20 = 0; /* selection of TAA0 counter clock = fXP1 */
   /* Interval timer mode setting */
   TAA0CTL0 = 0x00;          /* fxp1: 4MHZ */
   TAA0CTL1 = 0x00;          /* Intervall Timer */
   TAA0CCR0 = 0xffff;          /* Vergleichswert wird durch ModBus Status Maschine gesetzt */


   /* RS 485 Transceiver Chip LTC 1535CSW: TX und Rx enable */
   /* PCM2 und PCM3 als Ausgang */
   PMCM = 0xFF & (~0x0C);
   /* PCM im Port Mode */
   PMCCM = 0x00;
   /* PCM2 low, PCM3 high */
   MODBUS_TX_OFF_RX_OFF


   /*  UART0 starten
     RX Interrupt und Status Interrupt aktivieren
     TX Interrupt bleibt aus, es wird auf das Interuptflag gepollt
   */
   UD0TIF = 0; /* clear INTUD0T interrupt flag */
   UD0RIF = 0; /* clear INTUD0R interrupt flag */
   UD0RMK = 0; /* INTUD0R interrupt enable */
   UD0SIF = 0; /* clear INTUD0S interrupt flag */
 //  UD0SMK = 0; /* INTUD0S interrupt enable */

   UD0PWR = 1; /* enable UARTD0 operation */
   UD0TXE = 1; /* enable transmission operation(uartd0) */
   UD0RXE = 1; /* enable reception operation(uartd0) */

   MODBUS_SET_TIMER_VALUE(u16_FramePauseTimerValue)

   u16_BusDebugState |= MOD_BUS_DBG_DATA_LINK_INIT_UART_EXIT;
   return(MOD_BUS_OK);
#else
		return(MOD_BUS_INIT_FAIL);
#endif

}

/**
 *****************************************************************************
 * @brief       The Modbus message receive Interrupt
 * @note        INTUD0R id UART0 reception interrupt
 *              Bytes are written to Array 'au8_ReadData' and are kept
 *              there until a complete frame is read, the inter byte time out
 *              elapses
 * @param[in]   none
 * @return      none
 *****************************************************************************
 */
#pragma vector = INTUD0R_vector
__interrupt void MD_INTUD0R(void)
{
  u16_BusDebugState |= MOD_BUS_DBG_DATA_LINK_UART_RX_ISR;
  
  while(1)
  {
// 31.10.2012, VHildebrandt
// kein Watchdog in ISR !!!!!
//    mWATCHDOG();
    // neuer UART Status ?
    if(UD0SIF)
    {
      u8_UartErrorType = (uint8_t)(UD0STR & 0x07);
      UD0STR = (uint8_t)~0x07;
      UD0SIF = 0;
            au8_ReadData[0] = UD0RX;
      if(u8_UartErrorType)
      {
        break;
      } // endif
    } // endif

    // neues Byte zum Einlesen ?
    if(UD0RIF || u8_NumReadBytes == 0)
    {
      // eingelesenes Byte in Buffer kopieren
      if(u8_NumReadBytes < (NUM_MOD_BUS_DATA_BYTES + 4))
      {
        au8_ReadData[u8_NumReadBytes] = UD0RX;
                UD0RIF = 0;

        if(u8_NumRegBytes == 0 && u8_NumReadBytes == 1)
        {
          // durch Antwort wird Fehler signalisiert (MSB im Funktionscode gesetzt)
          if(au8_ReadData[u8_NumReadBytes] & 0x80)
          {
            // -> es folgende noch der Ausnahmecode und 2 x CRC, schon gelesen wurden Slave Adresse und Funktionscode
            u8_NumRegBytes = 5;
          } // endif
        } // endif
        // Anzahl erwarteter Bytes noch nicht bekannt und L�ngenbyte erreicht ??
        else if(u8_NumRegBytes == 0 && u8_NumReadBytes == 2)
        {
          u8_NumRegBytes = au8_ReadData[u8_NumReadBytes] + 5;  // +5 Slave Adr, Funccode und 2 x CRC + L�ngenbyte
        } // endif
        u8_BytesToEval++;
        u8_NumReadBytes++;
      } // endif

      /* erwartete Anzahl Bytes gelesen ? */
      if(u8_NumReadBytes < u8_NumRegBytes || u8_NumRegBytes == 0)
      {
        /* nein -> Interbyte Timeout Timer neu starten */
        MODBUS_SET_TIMER_VALUE(u16_InterbyteTimeoutValue)
      } // endif
      else
      { /* ja -> ModBus Frame ist komplett */
        MODBUS_TIMER_STOP
        break;
      } // endelse
    } // endif

    // Interbyte Timeout abgelaufen?
    if(MODBUS_TIMER_IRQ_FLAG)
    {
      u8_UartErrorType = MOD_BUS_INTERBYTE_TIMEOUT;
            break;
    } // endif
  } // endwhile

  UD0RMK = 1; /* INTUD0R interrupt disable */

}

/**
 *****************************************************************************
 * @brief       The Modbus status Interrupt
 * @note        INTUD0s is UART0 status interrupt
 * @param[in]   none
 * @return      none
 *****************************************************************************
 */
#pragma vector = INTUD0S_vector
__interrupt void MD_INTUD0S(void)
{

  u16_BusDebugState |= MOD_BUS_DBG_DATA_LINK_UART_STATE_ISR;

    /* Fehlerbits einlesen */
    u8_UartErrorType = (uint8_t)(UD0STR & 0x07);
    /* und im Statusregister zur�cksetzen */
    UD0STR = (uint8_t)~0x07;
    /* Dummy Read des fehlerhaften Bytes */
    au8_ReadData[0] = UD0RX;

}


/**
 *****************************************************************************
 * @brief       The communication state machine
 * @note
 * @param[in]   none
 * @return      none
 *****************************************************************************
 */
void BSP_Modbus_stateMachine( void )
{
  /* Kommunikation l�uft ? */
  if(pst_ModBusData -> state_enm != MOD_BUS_COMM_IN_PROGRESS)
  {
    if(MODBUS_TIMER_IRQ_FLAG)
    {
      /* Framepause abglaufen */
      /* -> ModBus Intervall Timer kann gestoppt werden */
      MODBUS_TIMER_STOP
      u8_FramePauseOk = TRUE;
    } // endif
    /* zur Sicherheit: */
    /* nach 5 * Antworttimeout eines Frames gilt die Frame Pause als eingehalten */
    else if(u16_100msFrameOkCnt >= u16_100msFrameOkValue)
    {
      u16_BusDebugState |= MOD_BUS_DBG_DATA_LINK_FRAME_PAUSE_ERR;
      u8_FramePauseOk = TRUE;
    } // endif
  } // endif

  switch(pst_ModBusData -> state_enm)
  {
    case MOD_BUS_COMM_IDLE:
      u16_BusDebugState |= MOD_BUS_DBG_DATA_LINK_STATE_IDLE;
      break;

    // Daten zum Senden aufbereiten und versenden
    case MOD_BUS_COMM_REQ_SET:
      u16_BusDebugState |= MOD_BUS_DBG_DATA_LINK_STATE_COM_REQ;
      // Frame Pause einhalten: Intervall Timer Zeit muss abgelaufen sein
      if(u8_FramePauseOk == TRUE)
      {
        u8_UartErrorType = 0;
        u8_NumReadBytes = 0;
        u8_NumExecBytes = 0;
        u8_BytesToEval = 0;
        u8_NumRegBytes = 0;
                u8_FramePauseOk = FALSE;

                // n�chster Status ist 'Antwort �berwachen'
                pst_ModBusData -> state_enm = MOD_BUS_COMM_IN_PROGRESS;

                // Antwort Timeout initialsieren
                u16_100msCounter = (uint16_t) ((FLP32) (pst_ModBusData -> req_timeout_u16) / 100.0 + 0.5);

        /* erwartete Anzahl Antwortbytes vorgegeben ? */
        if(pst_ModBusData -> num_read_data_bytes_u16 > 0)
        {
          /* -> Anzahl fuer Freigabe des CAN Interrupts merken */
          u8_NumRegBytes = (pst_ModBusData -> num_read_data_bytes_u16) + 4;  // +4 : Slave  Adr, Func Code un 2 x CRC
        } // endif

                // Daten versenden
                SendData(pst_ModBusData);

        // nach 5 * Timeout 100 ms Z�hler soll die Framepause als in jedem Fall eingehalten gelten
        u16_100msFrameOkValue = 5 * (uint16_t) ((FLP32) (pst_ModBusData -> req_timeout_u16) / 100.0 + 0.5);
        // es wurde ein Frame versendet => 100 ms Intervall Z�hler initialisieren
        u16_100msFrameOkCnt = 0;
      } // endif

      break;

    case MOD_BUS_COMM_IN_PROGRESS:
      u16_BusDebugState |= MOD_BUS_DBG_DATA_LINK_STATE_COM_PROGR;
      // min. ein neues Byte gelesen
      if( u8_BytesToEval > 0 )
      {
        // Parity Error beim Empfang ?
        if(u8_UartErrorType > 0)
        {
          // -> Fehler
          pst_ModBusData -> state_enm = MOD_BUS_COMM_READ_ERROR;
          pst_ModBusData -> error_code = MOD_BUS_UART_READ_ERR;
          u16_BusDebugState |= MOD_BUS_DBG_DATA_LINK_UART_ERR;
        } // endif

        // Neue Telegrammbytes empfangen
        // bis hier kein Fehler erkannt ?
        while(u8_BytesToEval > 0)
        {
          if(pst_ModBusData -> state_enm == MOD_BUS_COMM_IN_PROGRESS)
          {
            // gelesene Bytes weiterverarbeiten
            eval_read_bytes(pst_ModBusData, &au8_ReadData[u8_NumExecBytes], &u8_NumExecBytes, u8_BytesToEval);

          } // endif

          u8_BytesToEval = 0;
          mWATCHDOG();
        } // endwhile
      } // endif

      // Falls noch Bytes fehlen, Timeout f�r Empfang eines Frames �berwachen
      if( u16_100msCounter == 0 && pst_ModBusData -> state_enm == MOD_BUS_COMM_IN_PROGRESS)
      {
        pst_ModBusData -> state_enm = MOD_BUS_COMM_READ_ERROR;
        pst_ModBusData -> error_code = MOD_BUS_TIMEOUT;
        u16_BusDebugState |= MOD_BUS_DBG_DATA_LINK_TIMEOUT_ERR;
      } // endif

            // Falls Kommunikation abgeschlossen Timer f�r Frame Pause initialsieren
      if (pst_ModBusData -> state_enm != MOD_BUS_COMM_IN_PROGRESS)
      {
        MODBUS_SET_TIMER_VALUE(u16_FramePauseTimerValue)
      } // endif
      break;
  } // endswitch

} /* end of mod_bus_status_maschine */


#endif  //BSP_USE_V850 == 1
/* End of BSP_MODBUS_V850.c ==================================================== */
