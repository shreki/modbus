#include <typ_def.h>
#include <data_link_layer_common_ext.h>
#include <data_link_layer_common.h>

#ifdef PC_VERSION
	#include HEADER_MB_DATA_LINK_PC
#else	
    #include "BSP.h"
#endif

/** @brief allg. Data Link Layer Funktionen
*   @file data_link_layer_common.c
*
*   beinhaltet die Schnittstelle zum Application Layer sowie 
*   zur hardwarespezifischen Data Link Layer Schicht
*
*   @author Volker Hildebrandt
*   @date   08.05.2012
*   @date   Letzte Aenderung: 08.05.2012 - Hildebrandt
*/

static mod_bus_data_def_strct mod_bus_data_strct = {0};

/* Table of CRC values for high�order byte */
static uINT8 CRCHi_pu8[] = {
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81,
0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40
} ;

/* Table of CRC values for low�order byte */
static uINT8 CRCLo_pu8[] = {
0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4,
0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD,
0x1D, 0x1C, 0xDC, 0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7,
0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE,
0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2,
0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB,
0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0, 0x50, 0x90, 0x91,
0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88,
0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80,
0x40
};


/** @brief errechnen der Checksumme eines ModBus Telegramms
*  	
*   @param mod_bus_data_pstrct      Zeiger Frame Daten
*   @param num_bytes      	        Anzahl Bytes in 'msg_data_pu8'
*
*   @return                  2 Byte CRC-16 Wert
*/

uINT16 calc_CRC16 ( mod_bus_data_def_strct* mod_bus_data_pstrct, uINT16 num_bytes_u16)
{
	uINT8 CRCHi_val_u8 = 0xFF ; 
	uINT8 CRCLo_val_u8 = 0xFF ; 	
	uINT16 index_u16 ; 
	uINT16 byte_cnt_u16 = 0;		
  
  // Slave Adr
	index_u16 = CRCLo_val_u8 ^ (mod_bus_data_pstrct -> slave_adr_u8);
	CRCLo_val_u8 = CRCHi_val_u8 ^ CRCHi_pu8[index_u16];
	CRCHi_val_u8 = CRCLo_pu8[index_u16];

  // Func Code
	index_u16 = CRCLo_val_u8 ^ (mod_bus_data_pstrct -> func_code_u8);
	CRCLo_val_u8 = CRCHi_val_u8 ^ CRCHi_pu8[index_u16];
	CRCHi_val_u8 = CRCLo_pu8[index_u16];
		
	while (num_bytes_u16--)	
	{
 		index_u16 = CRCLo_val_u8 ^ ((mod_bus_data_pstrct -> data_u8)[byte_cnt_u16]);
		CRCLo_val_u8 = CRCHi_val_u8 ^ CRCHi_pu8[index_u16];
		CRCHi_val_u8 = CRCLo_pu8[index_u16];
		
		byte_cnt_u16++;
	} // endwhile
	return (CRCHi_val_u8 << 8 | CRCLo_val_u8);
} /* end of calc_CRC16 */


/** @brief Gelesene Bytes weiterverarbeiten
*
*   die eingelesenen Datenbytes weiterverarbeiten
*   Dabei wird in 'num_read_bytes_pu8' die Anzahl der bisher
*   innerhalb des Gesamtframes verarbeiteten Bytes verwaltet. Ein
*   Frame kann aber in mehreren Lesevorg�ngen eingelesen werden
*   Der Index innerhalb des aktuellen Lesevorgangs ist 'byte_index_u32'
*
*   @param mod_bus_data_strct  		Struktur mit ModBus Daten
*   @param read_data_u8  			    gelesene Datenbytes
*   @param num_read_bytes_pu8		  Anzahl bisher gelesene Datenbytes
*   @param bytes_to_read_u32  		Anzahl neu gelesener Datenbytes
*
*	@return							-
*/	

void eval_read_bytes(mod_bus_data_def_strct* mod_bus_data_pstrct, uINT8* read_data_u8, uINT8* num_bytes_exec_pu8, uINT8 bytes_read_u8)
{
	uINT8 byte_index_u8 = 0;
	uINT8 i;
	uINT16 ist_crc16_u16;
	uINT16 soll_crc16_u16;
	static uINT8 crc_hi_byte_u8 = 0;
	static uINT8 crc_lo_byte_u8 = 0;
		
	// 1. Byte ist Slave Adresse
	if( *num_bytes_exec_pu8 == 0)  // Slave Adresse noch nicht ausgewertet ?
	{
		// Antwortet der Slave mit seiner Adresse ?
		if(read_data_u8[0] != mod_bus_data_pstrct -> slave_adr_u8)
		{
			// nein -> Fehler
			mod_bus_data_pstrct -> state_enm = MOD_BUS_COMM_READ_ERROR;
			mod_bus_data_pstrct -> error_code = MOD_BUS_INVALID_SLAVE;
			return;
		} // endif					
		
		(*num_bytes_exec_pu8)++;
		byte_index_u8++;
	} // endif
	
	if(bytes_read_u8 > byte_index_u8) // noch unverarbeitete Bytes vorhanden ?
	{
		if( *num_bytes_exec_pu8 == 1)     // Func Code noch nicht ausgewertet
    {
			// handelt es sich um einen Error Frame (urspr�ngliche Funktionscode + 0x80 ?
			if( read_data_u8[byte_index_u8] & 0x80 )
			{
				// -> es folgt noch 1 Byte mit dem Exception Code
				mod_bus_data_pstrct -> num_read_data_bytes_u16 = 1;
			} // endif
		
			mod_bus_data_pstrct ->	func_code_u8 = read_data_u8[byte_index_u8];
			(*num_bytes_exec_pu8)++;
			byte_index_u8++;									
		} // endif		
	} // endif
	
	
	// ggfs. die Anzahl Datenbytes in der Antwort bestimmen
	if( (bytes_read_u8 > byte_index_u8)                       // noch unverarbeitete Bytes vorhanden ?
	&&  (mod_bus_data_pstrct -> num_read_data_bytes_u16 == 0)   // kein Sollbyte Anzahl vorgegeben ?
	&&  (*num_bytes_exec_pu8) == 2 )                            // 3. Byte ist die Anzahl Datenbytes
	{
	  (mod_bus_data_pstrct -> num_read_data_bytes_u16) = (uINT16) read_data_u8[byte_index_u8] + 1;  // +1 das L�ngenbyte selbst z�hlt auch mit !!
	} // endif 
		  
	// noch unverarbeitete Bytes vorhanden und Checksummenbytes noch nicht erreicht?
	if(bytes_read_u8 > byte_index_u8      // noch unverarbeitete Bytes vorhanden ?
	&& (*num_bytes_exec_pu8) >= 2         // Anzahl Datenbytes ist bekannt ? 
	&& (*num_bytes_exec_pu8) < (mod_bus_data_pstrct -> num_read_data_bytes_u16) + 2)    // noch nicht alle Datenbytes verarbeitet
	{
	  // -> Daten kopieren
		for(i = byte_index_u8; i < bytes_read_u8; i++)	
		{
			(mod_bus_data_pstrct -> data_u8)[(*num_bytes_exec_pu8) - 2] =  read_data_u8[i];	
			(*num_bytes_exec_pu8)++;
			byte_index_u8++;	
			
			// Anzahl erwartete Lesebytes erreicht ?
			if(*num_bytes_exec_pu8 == (mod_bus_data_pstrct -> num_read_data_bytes_u16) + 2)		// + 2 wg. Slave Adresse und Funktionscode
			{
				break;
			} // endif								
		} // endfor
	} // endif
  
  // ab hier Checksummenbytes erreicht
  
  // CRC Low Byte	
	if( bytes_read_u8 > byte_index_u8        // noch unverarbeitete Bytes vorhanden ?
	&& (*num_bytes_exec_pu8) == (mod_bus_data_pstrct -> num_read_data_bytes_u16) + 2)	
	{
	  crc_lo_byte_u8 = read_data_u8[byte_index_u8];
	  byte_index_u8++;
	  (*num_bytes_exec_pu8)++;
	} // endif

  // CRC High Byte
	if(bytes_read_u8 > byte_index_u8        // noch unverarbeitete Bytes vorhanden ?
	&& (*num_bytes_exec_pu8) == (mod_bus_data_pstrct -> num_read_data_bytes_u16) + 3)	
	{
	  crc_hi_byte_u8 = read_data_u8[byte_index_u8];
	  byte_index_u8++;
	  (*num_bytes_exec_pu8)++;
	} // endif
	
	// Checksummenbytes auswerten	
	if( (*num_bytes_exec_pu8) >= (mod_bus_data_pstrct -> num_read_data_bytes_u16) + 4)	
	{
		// Soll CRC errechnen
		soll_crc16_u16 = calc_CRC16( mod_bus_data_pstrct, mod_bus_data_pstrct -> num_read_data_bytes_u16);
		
		// Ist CRC errechnen
		ist_crc16_u16 = 256 * (uINT16) crc_hi_byte_u8 + (uINT16) crc_lo_byte_u8;
		
		// Stimmt die CRC ?
		if(soll_crc16_u16 == ist_crc16_u16)
		{
			mod_bus_data_pstrct -> state_enm = MOD_BUS_COMM_DATA_VALID;	
	    crc_hi_byte_u8 = 0;
	    crc_lo_byte_u8 = 0;			
			
		} // endif
		else
		{
		  // Checksummenfehler
			mod_bus_data_pstrct -> state_enm = MOD_BUS_COMM_READ_ERROR;	
			mod_bus_data_pstrct -> error_code = MOD_BUS_CRC_ERR;
			crc_hi_byte_u8 = 0;
			crc_lo_byte_u8 = 0;			
		} // endelse		
	} // endif	
		
} /* end of eval_read_bytes */


/** @brief Initialisierung der UART Schnttstelle
*
*	Schnittstelle zum Application Layer f�r die Initialisierung
*	des ModBus Treibers
*
*	Diese Funktion verwzeigt in die hardwarespezifischen
* 	Initialsierungsroutinen
*
*   @param baudrate_enm         	Baudrate
*   @param parity_enm      			Paritaet
*   @param interbyte_timeout_u16   	max. Zeit zwischen 2 Zeichen in us 
*									(0 -> Wert wird anhand der  Baudrate ausgew�hlt)
*   @param frame_pause_u16   		min. Zeit zwischen 2 Datenframes in us 
*									(0 -> Wert wird anhand der  Baudrate ausgew�hlt)
*									
*   @return                     	MOD_BUS_OK 		 -> Initalisierung ok
									MOD_BUS_INIT_FAIL -> Initalisierung fehlgeschlagen									
*/

sINT8 init_mod_bus(baudrate_def_enm baudrate_enm, parity_def_enm parity_enm, uINT16 interbyte_timeout_u16, uINT16 frame_pause_u16)
{
	sINT8 status_u8;
	uINT16 wert_u16;
	uINT16 baudrate_u16;	

	//mod_bus_data_link_layer_set_dbg_state(MOD_BUS_DBG_DATA_LINK_INIT_ENTRY);
		
	/** Timeout bzw. Framepause in HW Data Link Layer setzen */	
	if(interbyte_timeout_u16 > 0)
	{
		// Timeout aus Application Layer vorgegeben ?
		set_interbyte_timeout(interbyte_timeout_u16);
	} // endif
	else
	{
		// Timeout errechnen (t1.5 char)

		switch(baudrate_enm)
		{
		
			case MOD_BUS_BAUD_9600:
				baudrate_u16 = 9600;
			break;

			case MOD_BUS_BAUD_19200:
			case MOD_BUS_DEFAULT_BAUD:
				baudrate_u16 = 19200;
			break;
			
		default:	
			baudrate_u16 = 0xFFFF;
			break;
		} // endswitch				
			
		if(baudrate_u16 == 0xFFFF)
		{
			return(MOD_BUS_INIT_FAIL);
		} // endif
		
		// t.15 char errechnen
		// Bits pro Sekunde * (8 Bit pro Byte + 1 Startbit + 1 Paritaetbit+ 1 Stopbit ) * 1.5 in us
		wert_u16 = (uINT16) ((1000000.0 / (FLP32) baudrate_u16) * 11.0 * 1.5);
		set_interbyte_timeout(wert_u16);				
	} // endelse

	if(frame_pause_u16 > 0)
	{
		// Framepause aus Application Layer vorgegeben ?
		set_frame_pause(frame_pause_u16);
	} // endif
	else
	{
		switch(baudrate_enm)
		{
		
			case MOD_BUS_BAUD_9600:
				baudrate_u16 = 9600;
			break;

			case MOD_BUS_BAUD_19200:
			case MOD_BUS_DEFAULT_BAUD:
				baudrate_u16 = 19200;
			break;
			
		default:	
			baudrate_u16 = 0xFFFF;
			break;
		} // endswitch				
			
		if(baudrate_u16 == 0xFFFF)
		{
			return(MOD_BUS_INIT_FAIL);
		} // endif
		
		// t.35 char errechnen
		// Bits pro Sekunde * (8 Bit pro Byte + 1 Startbit + 1 Paritaetbit+ 1 Stopbit ) * 3.5 in us
		wert_u16 = (uINT16) ((1000000.0 / (FLP32) baudrate_u16) * 11.0 * 3.5);
		set_frame_pause(wert_u16);				

	} // endelse
	
	#ifdef PC_VERSION
		set_pc_com_port_nr(PC_COM_PORT);		/** Nummer des zu benutzenden Com Ports setzen */		
		/** Zeiger Semaphore Funktionen holen) */
		get_semaphore_func_ptr( &(mod_bus_data_strct.wait_comm_semaphore),		
							    &(mod_bus_data_strct.release_comm_semaphore));
	#endif
	
	/** im HW Layer die ModBus Daten Struktur bekanntmachen 
	(HW Layer speichert Zeiger auf die Variable */
	set_mod_bus_data_ptr(&mod_bus_data_strct);
	
	// serielle Schnittstelle initalisieren
	status_u8 =  init_mod_bus_uart(baudrate_enm, parity_enm);
	
	// Initislaisierung erfolgreich ?
	if(status_u8 == MOD_BUS_OK)
	{
		// ModBus kann jetzt benutzt werden
		
		/* ggfs. auf Zugriff warten */
		if(mod_bus_data_strct.wait_comm_semaphore)
		{
			(mod_bus_data_strct.wait_comm_semaphore)(0);
		} // endif
		
		mod_bus_data_strct.state_enm = MOD_BUS_COMM_IDLE;
		
		/* ggfs. auf Zurgiff weiter erlauben */
		if(mod_bus_data_strct.release_comm_semaphore)
		{
			(mod_bus_data_strct.release_comm_semaphore)();
		} // endif	
	} // endif
	
	//mod_bus_data_link_layer_set_dbg_state(MOD_BUS_DBG_DATA_LINK_INIT_EXIT);
	return(status_u8);
} /* end of init_mod_bus */

/** @brief ModBus Schreib/Lese Vorgang ausloesen
*
*	Schnittstelle zum Application Layer f�r die Ausloseung eines
* 	Lese/Schreibzugriffs durch den Application Layer
*
*	Diese Funktion verwzeigt in die hardwarespezifischen
* 	Routinen
*
*	  @param req_type_enm				Typ des Zugriffs
*   @param slave_adr_u8	         	Adresse des ModBus Teilnehmers
*   @param func_code_u8     		Funktionscode
*   @param num_write_bytes_u8   	Anzahl zu schreibender Bytes
*   @param write_data_pu8 		  	zu schreibende Datenbytes
*	  @param req_timeout				Timeout in [ms]
*									
*   @return                     	MOD_BUS_OK 		 	 ->	 Schreibvorgang erfolgreich
									MOD_BUS_NO_INIT	 	 -> ModBus noch nicht initialisiert
									MOD_BUS_BUSY_ERR  	 -> ModBus ist bereits belegt (ein Request laeuft)
									MOD_BUS_UART_ERR	 -> UART Fehler beim vorherigen Request		
									MOD_BUS_PARAM_ERR	 ->	ung�ltiger Parameterwert
									MOD_BUS_OVERRUN		 -> Daten wurden nicht schnell genug abgeholt
*/

sINT8 req_modbus_data(uINT8 slave_adr_u8,
                      uINT8 func_code_u8,
                      uINT8 num_write_bytes_u8,
                      uINT8 num_read_bytes_u8,
                      uINT8* data_pu8,
                      uINT16 req_timeout_u16)
{	
	mod_bus_comm_state_enm	state_enm;
	sINT8 status_u8 = MOD_BUS_OK;
	uINT8 i;
	
	//mod_bus_data_link_layer_set_dbg_state(MOD_BUS_DBG_DATA_LINK_REQ_ENTRY);
	
	/* ggfs. auf Zugriff warten */
	if(mod_bus_data_strct.wait_comm_semaphore)
	{
		(mod_bus_data_strct.wait_comm_semaphore)(0);
	} // endif
	
	state_enm = mod_bus_data_strct.state_enm;			
	/* ModBus noch nicht initialisiert */
	if( state_enm == MOD_BUS_COMM_NO_INIT)
	{
		status_u8 = MOD_BUS_NO_INIT;
	} // endif			
	else if( state_enm == MOD_BUS_COMM_WRITE_ERROR || state_enm == MOD_BUS_COMM_READ_ERROR)
	{
		status_u8 = mod_bus_data_strct.error_code;
		
		/* naechsten Zugriff erm�glichen */
		mod_bus_data_strct.state_enm = MOD_BUS_COMM_IDLE;
	} // endif
	/* Leseanforderung bevor Daten abgeholt werden */
	else if( state_enm == MOD_BUS_COMM_DATA_VALID )
	{
		status_u8 = MOD_BUS_OVERRUN;
		
		/* naechsten Zugriff erm�glichen */
		mod_bus_data_strct.state_enm = MOD_BUS_COMM_IDLE;		
	} // endif
	/* Anfrage ModBus l�uft schon ? */
	else if( state_enm == MOD_BUS_COMM_IN_PROGRESS || state_enm == MOD_BUS_COMM_REQ_SET )
	{
		status_u8 = MOD_BUS_BUSY_ERR;
    } // endif    
	else if( state_enm == MOD_BUS_COMM_IDLE )
	{
		/* ModBus ist frei 
		-> Schreibdaten zuweisen */		
		/* Anzahl Bytes g�ltig ?*/
		if( num_read_bytes_u8  > NUM_MOD_BUS_DATA_BYTES
		||  num_write_bytes_u8 > NUM_MOD_BUS_DATA_BYTES)
		{
			return(MOD_BUS_PARAM_ERR);
		} // endif
		
		// kleinster erlaubter Timeout ist 100 ms
		if(req_timeout_u16 < 100)
		{
			return(MOD_BUS_PARAM_ERR);
		} // endif
		
		mod_bus_data_strct.req_timeout_u16 = req_timeout_u16;
		mod_bus_data_strct.slave_adr_u8 = slave_adr_u8;
		mod_bus_data_strct.func_code_u8 = func_code_u8;		
		for(i = 0; i < num_write_bytes_u8; i++)
		{
			(mod_bus_data_strct.data_u8)[i] = data_pu8[i];
		} // endfor
		
		mod_bus_data_strct.num_write_data_bytes_u16 = num_write_bytes_u8;
		mod_bus_data_strct.num_read_data_bytes_u16 = num_read_bytes_u8;	
				
		/* Neuen Status f�r Kommunikationszustandsmaschine setzen */
		mod_bus_data_strct.state_enm = MOD_BUS_COMM_REQ_SET;
	} // endelse
		
	/* ggfs. auf Zurgiff weiter erlauben */
	if(mod_bus_data_strct.release_comm_semaphore)
	{
		(mod_bus_data_strct.release_comm_semaphore)();
	} // endif
	
	//mod_bus_data_link_layer_set_dbg_state(MOD_BUS_DBG_DATA_LINK_REQ_EXIT);
	return(status_u8);
} /* end of req_modbus_data */

/** @brief ModBus Schreib/Lese Ergbnis auslesen
*
*	Schnittstelle zum Application Layer f�r das Auslesen eines
* 	Lese/Schreibzugriffs durch den Application Layer
*
*	Diese Funktion verwzeigt in die hardwarespezifischen
* 	Routinen
*
*   @param func_code_pu8     		zur�ckgelesener Funktionscode
*   @param num_read_bytes_pu8   	Anzahl gelesene Bytes
*   @param read_data_pu8 		  	gelesene Datenbytes
*									
*   @return                     	MOD_BUS_OK 		 	 		-> Request erfolgreich, 'read_data_pu8' enth�lt Daten									
									MOD_BUS_NO_INIT				-> ModBus noch nicht initialisiert
									MOD_BUS_REQ_IN_PROGRESS		-> Request l�uft noch									
									MOD_BUS_UART_ERR			-> UART Fehler beim Schreiben bzw. Lesen der Daten																				
*/

sINT16 chk_modbus_data_req(uINT8* func_code_pu8, uINT16* num_read_bytes_pu16, uINT8* read_data_pu8)
{
	mod_bus_comm_state_enm	state_enm;
	sINT16 status_u8 = MOD_BUS_OK;	
	uINT8 i;
	
	//mod_bus_data_link_layer_set_dbg_state(MOD_BUS_DBG_DATA_LINK_CHK_ENTRY);
	
	/* ggfs. auf Zugriff warten */
	if(mod_bus_data_strct.wait_comm_semaphore)
	{
		(mod_bus_data_strct.wait_comm_semaphore)(0);
	} // endif

	state_enm = mod_bus_data_strct.state_enm;		
	/* ModBus noch nicht initialisiert */
	if( state_enm == MOD_BUS_COMM_NO_INIT)
	{
		status_u8 = MOD_BUS_NO_INIT;
	} // endif		
	else if( state_enm == MOD_BUS_COMM_WRITE_ERROR || state_enm == MOD_BUS_COMM_READ_ERROR)
	{
		status_u8 = mod_bus_data_strct.error_code;
		
		/* naechsten Zugriff erm�glichen */
		mod_bus_data_strct.state_enm = MOD_BUS_COMM_IDLE;
	} // endif
	/* Request ist noch nicht gestartet bzw. wird noch bearbeitet */
	else if( state_enm == MOD_BUS_COMM_REQ_SET || state_enm == MOD_BUS_COMM_IN_PROGRESS)
	{
		status_u8 = MOD_BUS_REQ_IN_PROGRESS;
	} // endif
	/* Timeout des Request abgelaufen */
	else if( state_enm == MOD_BUS_COMM_REQ_TIMEOUT )
	{
		status_u8 = mod_bus_data_strct.error_code;
		
		/* naechsten Zugriff erm�glichen */
		mod_bus_data_strct.state_enm = MOD_BUS_COMM_IDLE;				
	} // endif
	/*  Daten sind gueltig */
	else if( state_enm == MOD_BUS_COMM_DATA_VALID )
	{
		status_u8 = MOD_BUS_OK;
		
		/* Daten kopieren */
		*num_read_bytes_pu16 = mod_bus_data_strct.num_read_data_bytes_u16;
		*func_code_pu8 = mod_bus_data_strct.func_code_u8;				
		for(i = 0; i < mod_bus_data_strct.num_read_data_bytes_u16; i++)
		{
			read_data_pu8[i] = (mod_bus_data_strct.data_u8)[i];
		} // endfor
		
		/* naechsten Zugriff erm�glichen */
		mod_bus_data_strct.state_enm = MOD_BUS_COMM_IDLE;		
	} // endif
		
	/* ggfs. auf Zurgiff weiter erlauben */
	if(mod_bus_data_strct.release_comm_semaphore)
	{
		(mod_bus_data_strct.release_comm_semaphore)();
	} // endif
	
	//mod_bus_data_link_layer_set_dbg_state(MOD_BUS_DBG_DATA_LINK_CHK_EXIT);
	return(status_u8);	
} /* end of chk_modbus_data_req */


void close_mod_bus( void )
{

	
	#ifdef PC_VERSION
	// Mod Bus Schnittstelle schliessen
	close_mod_bus_uart();	
	
	// Mod Bus Daten Struktur zur�cksetzen
	memset(&mod_bus_data_strct, 0, sizeof(mod_bus_data_def_strct));
	#endif
} /* end of close_mod_bus */
